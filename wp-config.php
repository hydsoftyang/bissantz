<?php
/**
* The base configuration for WordPress
*
* The wp-config.php creation script uses this file during the
* installation. You don't have to use the web site, you can
* copy this file to "wp-config.php" and fill in the values.
*
* This file contains the following configurations:
*
* * MySQL settings
* * Secret keys
* * Database table prefix
* * ABSPATH
*
* @link https://codex.wordpress.org/Editing_wp-config.php
*
* @package WordPress
*/

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bissantz');

/** MySQL database username */
define('DB_USER', 'bissantz');

/** MySQL database password */
define('DB_PASSWORD', 'bissantz');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
* Authentication Unique Keys and Salts.
*
* Change these to different unique phrases!
* You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
* You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
*
* @since 2.6.0
*/
define('AUTH_KEY',         '0(so$9:k:%p{RGsz[6,eJI`L<M_r-QNK.Rit,6--&np&?e^}xg6p0R$?<n5L6uTY');
define('SECURE_AUTH_KEY',  'X}2>Qp!W&z+e|@pE-66ZOYV>&1N6W%p]rM9te0B]JYozga*g!4B%r8p8AjI$O-zM');
define('LOGGED_IN_KEY',    'jes<zS8f(cg`2e}N@SPRy(S7Q?XrR#2h!6N!+uNNxHlYc6(Ge-De^sZr?T-8LqS=');
define('NONCE_KEY',        '6dj_hbhq)l799zwPG&*T3;|QSq?(8x9_,{AlH[]NYD%G{+EP%hB9JL`OH*lvd zb');
define('AUTH_SALT',        '.cMR] !h=L|x`S=!]Ne+fygyYe9]j(OJL=;([iAD}.r>m]Y^CKdS726c_HUdNn?g');
define('SECURE_AUTH_SALT', 'j&BNBqrmX< oD^CH{6mj%y+&izEC8!h{z1DM.TH^H4}t?$mM1K?Xgn=zhxYwW}S.');
define('LOGGED_IN_SALT',   '=qXuUuHZl9W(ZAnr>;_EJ,@eTHpuSTC#HAT%}IX2o$&z61~?+i%)WT[BU]9$Q2)J');
define('NONCE_SALT',       '.XUQy{cux=k+/#8N7wr*9%QkHQ]HLgrp?w:|ET!z;.G;]B}lVQjk*r<k7rLOxar&');

/**#@-*/

/**
* WordPress Database Table prefix.
*
* You can have multiple installations in one database if you give each
* a unique prefix. Only numbers, letters, and underscores please!
*/
$table_prefix  = 'wp_';

/**
* For developers: WordPress debugging mode.
*
* Change this to true to enable the display of notices during development.
* It is strongly recommended that plugin and theme developers use WP_DEBUG
* in their development environments.
*
* For information on other constants that can be used for debugging,
* visit the Codex.
*
* @link https://codex.wordpress.org/Debugging_in_WordPress
*/
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

