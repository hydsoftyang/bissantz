<!DOCTYPE html>
<html lang="en" dir="ltr" xmlns:article="http://ogp.me/ns/article#" xmlns:book="http://ogp.me/ns/book#" xmlns:product="http://ogp.me/ns/product#" xmlns:profile="http://ogp.me/ns/profile#" xmlns:video="http://ogp.me/ns/video#" prefix="content: http://purl.org/rss/1.0/modules/content/  dc: http://purl.org/dc/terms/  foaf: http://xmlns.com/foaf/0.1/  og: http://ogp.me/ns#  rdfs: http://www.w3.org/2000/01/rdf-schema#  schema: http://schema.org/  sioc: http://rdfs.org/sioc/ns#  sioct: http://rdfs.org/sioc/types#  skos: http://www.w3.org/2004/02/skos/core#  xsd: http://www.w3.org/2001/XMLSchema# ">
<head>
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-TDT83SV');</script>
<!-- End Google Tag Manager -->
    <meta charset="utf-8" />
                <!-- Facebook Pixel Code -->
                <script>
                    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                    n.push=n;n.loaded=!0;n.version="2.0";n.queue=[];t=b.createElement(e);t.async=!0;
                    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                    document,"script","https://connect.facebook.net/en_US/fbevents.js");
                    fbq("init", "1371094059608456"); // Insert your pixel ID here.
                    fbq("track", "PageView");
                </script>
                <noscript><img height="1" width="1" style="display:none"
                    src="https://www.facebook.com/tr?id=1371094059608456&ev=PageView&noscript=1"
                    /></noscript>
                <!-- DO NOT MODIFY -->
                <!-- End Facebook Pixel Code --><meta name="title" content="Bissantz &amp; Company | Business Intelligence with DeltaMaster" />
<link rel="canonical" href="https://www.bissantz.com/" />
<link rel="shortlink" href="https://www.bissantz.com/" />
<meta name="description" content="Bissantz is specialized in software solutions for analysis, planning and reporting, and producer of the business intelligence suite DeltaMaster." />
<meta name="keywords" content="self service bi, bissantz.de, business intelligence company, bissantz &amp; company, business intelligence, bissantz, business intelligence software, DeltaMaster, olap datamining, bi reporting software" />
<meta name="referrer" content="no-referrer" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="shortcut icon" href="https://www.bissantz.com/themes/bissantz/favicon.ico" type="image/vnd.microsoft.icon" />
<link rel="alternate" hreflang="de" href="https://www.bissantz.de/node/1" />
<link rel="alternate" hreflang="en" href="https://www.bissantz.com/node/1" />

    <title>Bissantz &amp; Company | Business Intelligence with DeltaMaster</title>
    <link rel="stylesheet" href="https://www.bissantz.com/files/css/css_YX8ajw77snzNgVKP2rUeO9_qSYAF4QqU4TUFXdFLA_k.css?ozkdjx" media="all" />
<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:300,400,700" media="all" />
<link rel="stylesheet" href="https://www.bissantz.com/files/css/css_UMQBPMYs503puvFTqzib58CPpZUbNbhcM-nk8E-tmZY.css?ozkdjx" media="all" />


<!--[if lte IE 8]>
<script src="/files/js/js_VtafjXmRvoUgAzqzYTA3Wrjkx9wcWhjP0G4ZnnqRamA.js"></script>
<![endif]-->

</head>
<body  class="node-type-page-front front site-live">
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TDT83SV"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <!-- LinkedIn Insight-Tag -->
<script type="text/javascript"> _linkedin_data_partner_id = "110136"; </script><script type="text/javascript"> (function(){var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})(); </script> <noscript> <img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=110136&fmt=gif" /> </noscript>
<!-- End LinkedIn Insight-Tag -->
    <a href="#main-content" class="visually-hidden focusable">
        Skip to main content
    </a>

<header id="header">
    <a href="https://www.bissantz.com/">
        <h1 id="logo">Bissantz</h1>
    </a>
        <nav role="navigation" aria-labelledby="block-bissantz-main-menu-menu" id="block-bissantz-main-menu">

  <h2 class="visually-hidden" id="block-bissantz-main-menu-menu">Main navigation</h2>



            <div class="menu-wrapper level-1">
                    <ul class="menu level-1">
                                                <li class="menu-item level-1">
                <a href="https://www.bissantz.com/#deltamaster" data-drupal-link-system-path="&lt;front&gt;" class="is-active">DeltaMaster</a>
                            </li>
                                <li class="menu-item level-1">
                <a href="https://www.bissantz.com/#events" data-drupal-link-system-path="&lt;front&gt;" class="is-active">Meet</a>
                            </li>
                                <li class="menu-item level-1">
                <a href="https://www.bissantz.com/#references" data-drupal-link-system-path="&lt;front&gt;" class="is-active">Clients</a>
                            </li>
                                <li class="menu-item level-1">
                <a href="https://www.bissantz.com/#publications" data-drupal-link-system-path="&lt;front&gt;" class="is-active">Publications</a>
                            </li>
                                <li class="menu-item level-1">
                <a href="https://www.bissantz.com/#trainings" data-drupal-link-system-path="&lt;front&gt;" class="is-active">Trainings</a>
                            </li>
                                <li class="menu-item level-1">
                <a href="https://partner.bissantz.de/" ispopup="" class="link-extern">Partner</a>
                            </li>
                                <li class="menu-item level-1">
                <a href="https://www.bissantz.com/#contact" data-drupal-link-system-path="&lt;front&gt;" class="is-active">Contact</a>
                            </li>
                </ul>
        </div>



  </nav>


    <div id="debug-kaletsch"></div>
</header>

<div id="content">
    <section id="section-intro">
        <div class="aspect-wrapper">
            <div id="slider-intro" class="flexslider nav-responsive red">
                <ul class="slides">
                                        <!-- Teaser Executive Forum -->

                                                                                                <li class="slider-intro-generic-landingpage"
                                                                                    style="background-image: url(https://www.bissantz.com/files/slider-background/BI_Survey2017_Ergebnisse_en.png);
                                                                                                        background-position: center;
                                                                                                        background-size: cover;"
                                                                                >
                                    <div class="inner-slider">

    <div class="textbox ">
        <div class="inner whitebox">
            <div class="title">
                <p>DeltaMaster<br />
<em>in the BI Survey 17</em></p>

            </div>
        </div>
        <div class="link">
                        <a href="https://www.bissantz.com/node/531"
               class="button button-small">
                More information

            </a>
                    </div>
    </div>
</div>                                </li>
                                                                                                    <li class="slider-intro-generic-landingpage"
                                                                                >
                                    <div class="inner-slider">

    <div class="textbox ">
        <div class="inner whitebox">
            <div class="title">
                <p>Business Intelligence lab<br />
<em>on the race track&nbsp;</em></p>

            </div>
        </div>
        <div class="link">
                        <a href="https://www.bissantz.com/nordschleife_en"
               class="button button-small">
                More Info

            </a>
                    </div>
    </div>
</div>                                </li>
                                                                                                    <li class="slider-intro-generic-landingpage"
                                                                                    style="background-image: url(https://www.bissantz.com/files/slider-background/BissantzNumbers_en_responsive.png);
                                                                                                        background-position: center;
                                                                                                        background-size: cover;"
                                                                                >
                                    <div class="inner-slider">

    <div class="textbox ">
        <div class="inner whitebox">
            <div class="title">
                <p><span>Use cases, interview, and free</span><br />
<em>Add-in for Microsoft Excel</em></p>

            </div>
        </div>
        <div class="link">
                        <a href="https://www.bissantz.com/bissantznumbers_en"
               class="button button-small">
                More Info

            </a>
                    </div>
    </div>
</div>                                </li>

                    <!-- Teaser Testen -->

                    <!-- Hinweis Veranstaltung -->
                                    </ul>
            </div>
        </div>
    </section>

    <section id="section-deltamaster" class="blue">
        <div class="container-1200">
            <video id="video-bissantz-was" class="video-js vjs-default-skin vjs-big-play-centered vjs-fluid vjs-16-9"
                   preload="none" poster="https://www.bissantz.com/themes/bissantz/images/dm-was.jpg" style="background-image:url('/themes/bissantz/images/dm-was.jpg');"
                   data-setup='{"controls":true,"controlBar":{"volumeMenuButton":false},"plugins":{"center":{}}}'>
                <source src="https://www.bissantz.com/files/videos/hd/bc-deltamaster-en.webm" type='video/webm' />
<source src="https://www.bissantz.com/files/videos/hd/bc-deltamaster-en.mp4" type='video/mp4' />
<p class="vjs-no-js">
    To view this video please enable JavaScript, and consider upgrading to a web browser
    that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
</p>            </video>
        </div>
    </section>

    <section id="section-zitat1" class="quote">
        <div class="quote-inner">
    <div class="text">„Our calculations are much more precise with DeltaMaster. We can now detect unknown correlations on our own, and the automated analyses help us focus on what is truly important.“</div>
    <div class="from">Heike Wüsthoff, Gustav Alberts</div>
</div>
    </section>

    <section id="section-dm-wo" class="red">
        <div id="slider-videos-dm-wo" class="flexslider blue">
            <ul class="slides">
                                                    <li class="red">
                        <video id="video-wo-1" class="video-js vjs-default-skin vjs-big-play-centered vjs-fluid vjs-16-9"
                               preload="none" poster="https://www.bissantz.com/themes/bissantz/images/dm-wo-1.jpg" style="background-image:url('https://www.bissantz.com/themes/bissantz/images/dm-wo-1.jpg');"
                               data-setup='{"controls":true,"controlBar":{"volumeMenuButton":false},"plugins":{"center":{},"popup":{"btnClass":"bottom right","class":"blue","text":"Your entire company in your pocket? Anytime and anywhere? It’s possible with the DeltaMaster App which is optimized for touch interaction. The dashboard shows what is important at a glance. Get informative reports in the smallest of formats."}}}'>
                            <source src="https://www.bissantz.com/files/videos/hd/bc-iphone-en.webm" type='video/webm' />
<source src="/files/videos/hd/bc-iphone-en.mp4" type='video/mp4' />
<p class="vjs-no-js">
    To view this video please enable JavaScript, and consider upgrading to a web browser
    that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
</p>                        </video>
                    </li>
                                    <li class="red">
                        <video id="video-wo-2" class="video-js vjs-default-skin vjs-big-play-centered vjs-fluid vjs-16-9"
                               preload="none" poster="https://www.bissantz.com/themes/bissantz/images/dm-wo-2.jpg" style="background-image:url('https://www.bissantz.com/themes/bissantz/images/dm-wo-2.jpg');"
                               data-setup='{"controls":true,"controlBar":{"volumeMenuButton":false},"plugins":{"center":{},"popup":{"btnClass":"bottom right","class":"blue","text":"There are times when performance should really stand out. Sometimes you want to know what’s going on right now, right where you are. DeltaMaster projects performance indicators wherever you want them – on walls, in hallways, or on the floor. With distortion corrections and your choice of virtual frames."}}}'>
                            <source src="/files/videos/hd/bc-projections-en.webm" type='video/webm' />
<source src="/files/videos/hd/bc-projections-en.mp4" type='video/mp4' />
<p class="vjs-no-js">
    To view this video please enable JavaScript, and consider upgrading to a web browser
    that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
</p>                        </video>
                    </li>
                                    <li class="red">
                        <video id="video-wo-3" class="video-js vjs-default-skin vjs-big-play-centered vjs-fluid vjs-16-9"
                               preload="none" poster="https://www.bissantz.com/themes/bissantz/images/dm-wo-3.jpg" style="background-image:url('https://www.bissantz.com/themes/bissantz/images/dm-wo-3.jpg');"
                               data-setup='{"controls":true,"controlBar":{"volumeMenuButton":false},"plugins":{"center":{},"popup":{"btnClass":"bottom right","class":"blue","text":"Follow your company like a movie. See what is happening at your stores, production plants, or Web site. DeltaMaster visualizes complex processes. You get answers without first asking questions. Because we know: Your experienced eye is faster than most statistics."}}}'>
                            <source src="/files/videos/hd/bc-kiosksystems-en.webm" type='video/webm' />
<source src="/files/videos/hd/bc-kiosksystems-en.mp4" type='video/mp4' />
<p class="vjs-no-js">
    To view this video please enable JavaScript, and consider upgrading to a web browser
    that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
</p>                        </video>
                    </li>
                            </ul>
        </div>
    </section>

    <section id="section-zitat2" class="quote">
        <div class="quote-inner">
    <div class="text">„Our senior management uses DeltaMaster to detect outliers in KPIs and examine them without needing assistance from IT or management accounting.“</div>
    <div class="from">Soenke Stange, VELUX Deutschland</div>
</div>
    </section>

    <section id="section-dm-wie" class="">
        <ul class="wie-list">
                                                        <li class="red ">
                    <div class="container-1200 table">
                                                                                    <div class="square table-cell">
                                    Browse
                                </div>
                                                                                                                <div class="rect table-cell">
                                    <video id="video-wie-2"
                                           class="video-js vjs-default-skin vjs-big-play-centered vjs-big-play-small vjs-big-play-red vjs-fluid vjs-16-9"
                                           preload="none" poster="https://www.bissantz.com/themes/bissantz/images/dm-wie-browsen.jpg" style="background-image:url('https://www.bissantz.com/themes/bissantz/images/dm-wie-browsen.jpg')"
                                           data-setup='{"controls":true,"controlBar":false,"plugins":{"center":{},"popup":{"btnClass":"small red bottom left","class":"red","text":"Need to understand or present complex structures? With the Hyperbrowser in DeltaMaster, it is sheer child’s play. Using drag and drop, you can easily break down actuals or variances in these structures."}}}'>
                                        <source src="https://www.bissantz.com/files/videos/hd/bc-browse-en.webm" type='video/webm' />
<source src="https://www.bissantz.com/files/videos/hd/bc-browse-en.mp4" type='video/mp4' />
<p class="vjs-no-js">
    To view this video please enable JavaScript, and consider upgrading to a web browser
    that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
</p>                                    </video>
                                    <i class="arrow-left-red"></i>
                                </div>
                                                                        </div>
                </li>
                                            <li class="blue ">
                    <div class="container-1200 table">
                                                                                    <div class="rect table-cell">
                                    <video id="video-wie-1"
                                           class="video-js vjs-default-skin vjs-big-play-centered vjs-big-play-small  vjs-fluid vjs-16-9"
                                           preload="none" poster="https://www.bissantz.com/themes/bissantz/images/dm-wie-zoomen.jpg" style="background-image:url('https://www.bissantz.com/themes/bissantz/images/dm-wie-zoomen.jpg')"
                                           data-setup='{"controls":true,"controlBar":false,"plugins":{"center":{},"popup":{"btnClass":"small bottom right","class":"blue","text":"Our zoom does more than others. When you zoom in on sparklines and microcharts, DeltaMaster doesn’t just enlarge them. It fills the extra space with more information and details. Until a sparkline showing a trend becomes a full-fledged time series chart."}}}'>
                                        <source src="https://www.bissantz.com/files/videos/hd/bc-zoom-en.webm" type='video/webm' />
<source src="https://www.bissantz.com/files/videos/hd/bc-zoom-en.mp4" type='video/mp4' />
<p class="vjs-no-js">
    To view this video please enable JavaScript, and consider upgrading to a web browser
    that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
</p>                                    </video>
                                    <i class="arrow-right-blue"></i>
                                </div>
                                                                                                                <div class="square table-cell">
                                    Zoom
                                </div>
                                                                        </div>
                </li>
                                            <li class="red ">
                    <div class="container-1200 table">
                                                                                    <div class="square table-cell">
                                    Navigate
                                </div>
                                                                                                                <div class="rect table-cell">
                                    <video id="video-wie-2"
                                           class="video-js vjs-default-skin vjs-big-play-centered vjs-big-play-small vjs-big-play-red vjs-fluid vjs-16-9"
                                           preload="none" poster="https://www.bissantz.com/themes/bissantz/images/dm-wie-navigieren.jpg" style="background-image:url('https://www.bissantz.com/themes/bissantz/images/dm-wie-navigieren.jpg')"
                                           data-setup='{"controls":true,"controlBar":false,"plugins":{"center":{},"popup":{"btnClass":"small red bottom left","class":"red","text":"Are sales, costs, profits, quantities, or rates running as planned? Where are the variances - current and cumulated - to the last year, the average, or your budget? And why? DeltaMaster shows and explains variances and compensations even when they are hidden amongst millions of data records."}}}'>
                                        <source src="/files/videos/hd/bc-navigate-en.webm" type='video/webm' />
<source src="/files/videos/hd/bc-navigate-en.mp4" type='video/mp4' />
<p class="vjs-no-js">
    To view this video please enable JavaScript, and consider upgrading to a web browser
    that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
</p>                                    </video>
                                    <i class="arrow-left-red"></i>
                                </div>
                                                                        </div>
                </li>
                    </ul>
    </section>

    <section id="section-zitat3" class="quote">
        <div class="quote-inner">
    <div class="text">„High performance, fast implementation - that&#039;s why we chose DeltaMaster.“</div>
    <div class="from">Oliver Kissel, Daimler</div>
</div>
    </section>

    <section id="section-highlights" class="red">
        <div id="block-highlights" class="block block-1200 block-default">
            <h2>Highlights</h2>
            <div class="content">

<div id="slider-highlights" class="slider slick-default fixHeight">
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title red">
                    <span>01</span>
                    Intelligent dashboards
                </div>
                <div class="body">
                    <p>A dashboard in DeltaMaster serves as a gateway to all your analysis and reporting applications. You can assign a main KPI - for example, incoming orders, gross margin, pipeline, or close rates - to every application. DeltaMaster will then trans­­form it into an impressive, color-coded overview of your current business per&shyformance so you know which topics you should focus on first. DeltaMaster even takes care of the parameterization, configuration, and layout for you.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-0 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title">
                    <span>02</span>
                    Standard Reporting
                </div>
                <div class="body">
                    <p>Every&nbsp; company&nbsp; needs&nbsp; standard&nbsp; reports&nbsp; to&nbsp; steer performance,&nbsp; analyze&nbsp; variances,&nbsp; and&nbsp; forecast future business activities. With DeltaMaster, you can generate them in minutes with the help of the start wizard. Using drag-and-drop functions, you simply choose the desired methods to measure performance - for example, gross margin calculations, P&amp;L, or input-output calculations in shortand long-term views. The wizard will auto­matically integrate all necessary struc­tures to dynamically calculate cumula&shy;tions and variances to the budget or previous year into your data model. Many other so-called BI tools can't even do that manually, let alone automatically.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-1 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title red">
                    <span>03</span>
                    Integrated forecast
                </div>
                <div class="body">
                    <p>With a single click, you can create forecasts and projections for all actuals in DeltaMaster. Choose from simple, robust methods such as exponential smoothing and linear regression, values calculated from external modules, or a custom method developed by our forecasting experts. You can also create custom forecast models with the help of simple dialog boxes and powerful MDX queries.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-2 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title">
                    <span>04</span>
                    Open data storage
                </div>
                <div class="body">
                    <p>For enterprise reporting and KPI monitoring, you use the interfaces to the usual data warehouse systems of the major vendors. Unlike other products, DeltaMaster does not use proprietary data storage. Many of our clients value this independency. We support the following databases: Microsoft SQL Server/Analysis Services, SAP BW/ Netweaver BI/HANA, IBM Cognos TM1, Oracle OLAP, Oracle Essbase, Infor.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-3 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title red">
                    <span>05</span>
                    Flexibility
                </div>
                <div class="body">
                    <p>With DeltaMaster, you can design highly individualized applications for sales, purchasing, production, logistics, finance, marketing, CRM, after sales, sales planning, quality assurance, field sales management, etc. Since it is a standard product, all clients receive the exact same software and all existing applications stay compatible following a system update. We also integrate specific requests into the system so that our entire client base profits from the added functionality.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-4 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title">
                    <span>06</span>
                    Collaboration
                </div>
                <div class="body">
                    <p>DeltaMaster helps you share insights quickly and easily. Add extensive comments, send a report spontaneously, or work with other report editors simultaneously on the same application with the RepositoryService.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-5 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title red">
                    <span>07</span>
                    Role-based access
                </div>
                <div class="body">
                    <p>DeltaMaster manages data usage and access in a role-base access model. Executives and supervisory board members love DeltaMaster for its clear, precise,&nbsp; performance&nbsp; control&nbsp; templates&nbsp; and&nbsp; reports that work just as well on paper or a tablet as on a PC. Managing directors and department managers appreciate the option to explore details - either through pre-defined analysis paths or automatically generated paths in the result hierarchies. When asked to look into more specific questions, executive assistants and support teams are pleased that they can probe even deeper or switch perspectives all within the same tool. This supports the smooth transition&nbsp; between&nbsp; roles&nbsp; and&nbsp; responsibilities&nbsp; that is typical in modern companies. DeltaMaster RepositoryService ensures that data and roles fit together.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-6 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title">
                    <span>08</span>
                    Ad hoc analysis
                </div>
                <div class="body">
                    <p>DeltaMaster gives your management support teams the leeway they need to conduct creative, experimental analyses. This includes slice-and-dice or pivoting functions to compare and analyze custo­mers, regions, products, or other attri­butes on any level. You can group and classify objects by attributes dynamically or with fixed thresholds - regardless of their values. Wizards help you calculate quotients, sums, variances, or multiva­riate statistical values. Using drag-and- drop functions, you can visualize the results with circle, bar, or column elements. You can even apply differences to averages, cumulated variances, and other complex calculations to existing data with just a click of a button.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-7 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title red">
                    <span>09</span>
                    Business methods
                </div>
                <div class="body">
                    <p>DeltaMaster offers a selection of tried-and-tested business methods. These include rankings, time series, portfolio, concentration, map, location, and cross-table analyses, early warning signals, and drill-through functions to individual postings. These methods require no parametrization and are designed to work on every data model.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-8 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title">
                    <span>10</span>
                    Mobility
                </div>
                <div class="body">
                    <p>DeltaMaster flexibly accommodates the unique features of modern output media. Most people, for example, view mobile devices in portrait mode but standard computer screens in landscape mode. Both the design templates and functional concepts in DeltaMaster  take  this  into  account.  The  reports of DeltaMaster look just as impressive on the big screens of laptops, desktops, kiosk systems, and business control centers as the smaller displays of smartphones and tablets. You can centrally store data and applications in the cloud and use them both online or offline.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-9 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title red">
                    <span>11</span>
                    Data Mining
                </div>
                <div class="body">
                    <p>DeltaMaster brings the power of data mining to business users. This helps you conduct analyses without knowing the objective beforehand. Instead, the data should reveal it. With DeltaMaster, setting the necessary parameters is simple. You get results and clear explanations that you can simply pass on to management without any additional work. Supported methods include multidimensional rankings, component comparisons, shopping cart analyses, and Bayes method.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-10 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title">
                    <span>12</span>
                    Self-service BI
                </div>
                <div class="body">
                    <p>DeltaMaster supports spontaneous self-service analysis - even on new pools of data. Once you have imported your Excel, Access, or other relational sources, you can get right to work. During the modeling process, DeltaMaster automa­tically recognizes measures and dimen&shy;sions as well as which levels belong to a hierarchy. Best of all, users don't require IT support or special technical skills. With just a few steps, they can access their data in a clean, structured model for further analysis.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-11 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title red">
                    <span>13</span>
                    Guided Analytics
                </div>
                <div class="body">
                    <p>DeltaMaster automatically enters the suitable charts in your evaluations and suggests the correct graphical visuali&shy;zation for any tables. An analysis wizard examines cross tables for outliers using cluster analysis and other methods. To apply more complex methods such as distribution or regression analysis, you only need a few prior instructions. No programming is necessary.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-12 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title">
                    <span>14</span>
                    Signaling
                </div>
                <div class="body">
                    <p>The improved signaling in DeltaMaster helps you find your whereabouts quickly. It avoids the ambiguity of traffic-light functions, uses more effective colors from a physiological standpoint, and requires no parameters. DeltaMaster colors all values and graphic elements in shades of blue and red - depending on if they have a positive or negative effect on a specific KPI or your business as a whole. It applies this same principle to the program interface, edge of the screen, report lists, headlines, and other elements. This communicates a clear message that you cannot miss - even with just a quick glance at your desk or from afar on a kiosk system or business control  center.  What's  more,  DeltaMaster  always lists the values that are primarily responsible for the respective coloring.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-13 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title red">
                    <span>15</span>
                    Analysis paths
                </div>
                <div class="body">
                    <p>Where is room for improvement? Starting from the KPIs of your dashboard, DeltaMaster will guide you to those exact areas - whether in your product range, organization, suppliers, clients, or cost centers. DeltaMaster applies award-winning methods, for example, that document a chain of underlying causes. You can define typical paths as a predefined succession of clicks and keep sight of both the starting point and path travelled.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-14 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title">
                    <span>16</span>
                    Statistics
                </div>
                <div class="body">
                    <p>DeltaMaster  contains  the  statistical  functions  you need to conduct special analyses on campaigns, web visits, quality, or customer surveys. Using wizards you  can  create  univariate  and  multivariate  KPIs. Box plots help visualize prices and other ranges. DeltaMaster draws trend arrows and concentration curves - correctly, neatly, and fully automatically. Separate modules for distribution and regression analyses are also available.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-15 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title red">
                    <span>17</span>
                    Easy handling
                </div>
                <div class="body">
                    <p>DeltaMaster is a drag-and-drop product. All important  steps  you  need  to  use  and  create  reports work with simple, intuitive mouse movements. Our menus have very few icons. Instead, they display the effect of a command in plain words. Whereas other vendors cannot make up their minds, we work with robust, tried-and- tested default settings for colors, scales, captions, layout, etc. DeltaMaster requires fewer buttons and clicks than other BI products - on all user levels.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-16 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title">
                    <span>18</span>
                    Visual Analytics
                </div>
                <div class="body">
                    <p>The patented Hyperbrowser of DeltaMaster sheds light on complex structures - quickly and securely. You simply drag and drop a measure into the data model, and DeltaMaster will color code it to show if it has a positive or negative effect on the result. This instantly reveals compensating effects hidden in your data. The graphic tables in DeltaMaster make data-dense reports understandable at a glance. Interactive in-cell charts such as zoomable sparklines deliver more details as they grow larger.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-17 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title red">
                    <span>19</span>
                    Data entry and distribution
                </div>
                <div class="body">
                    <p>The integrated planning functionality in DeltaMaster completes the cycle of planning, analysis, and reporting. The software supports top-down and bottom- up data entry on cell and aggregated levels as well as fixed values and multi- level cell comments. Intelligent cells „know“ how to allocate entries. You can instantly integrate multidimensional, real-time analyses in the planning application to test the plausibility during the data-entry process and gauge the effects on the results. You can also display the planning status of different organizational units in the dashboard.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-18 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title">
                    <span>20</span>
                    ReportServer
                </div>
                <div class="body">
                    <p>DeltaMaster automatically  generates  personalized reports - even for hundreds of users. Based on templates that you have generated, DeltaMaster reloads the data, applies colors and warning signals, adapts reports to user-specific criteria, and takes care of exporting, emailing, or storing the file. You can trigger this process with a click of a button, at a set time (e.g. daily, weekly, monthly) or following an event (e.g. exception reporting).</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-19 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title red">
                    <span>21</span>
                    Portal
                </div>
                <div class="body">
                    <p>You can display all DeltaMaster appli­cations centrally in a separate, auto&shy;matically configured portal or integrate them in leading portal systems such as Microsoft SharePoint. In combi­na­tion with DeltaMaster Repository, the portal applies the valid permissions and roles for individual users. Single sign-on options are supported.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-20 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title">
                    <span>22</span>
                    Cloud BI
                </div>
                <div class="body">
                    <p>DeltaMaster delivers powerful BI for both headquarters and branch offices. Delta&shy;Master Repository administers all users, reports, and permissions and applies them to Windows, Web, and mobile environments. This ensures consistent applications that are easy to update and maintain. You can also use DeltaMaster offline, which is especially useful in global planning and budget consolidation processes.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-21 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title red">
                    <span>23</span>
                    Kiosk systems and business control centers
                </div>
                <div class="body">
                    <p>DeltaMaster makes the information in your company accessible and instantly usable without any setup times and little to no interaction. We offer solutions for kiosk systems, monitor walls, and busi&shy;ness control centers. Mobile users do not need to take their devices with them - there are devices ready where you need them. You can also integrate images, films, Web sites, and video streams as part of a multimedia corporate commu&shy;nications concept.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-22 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title">
                    <span>24</span>
                    Office-Integration
                </div>
                <div class="body">
                    <p>You can export DeltaMaster analyses and reports to HTML, PDF, Word, Excel, and PowerPoint files. For writing-intensive reports with custom designs, DeltaMaster also offers a live integration in Word. With this add-in, you can transfer reports and report elements down to individual cells and update them dynamically. This functionality is also available for PowerPoint.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-23 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title red">
                    <span>25</span>
                    Innovation
                </div>
                <div class="body">
                    <p>In corporation with Germany's largest corporations and midsize companies, we conduct research in a variety of fields including executive management, employee motivation, perception/visuali­zation, and data analysis/forecasting. These insights flow into best practice applications that serve as prototypes for new solutions. To ensure that our R&amp;D profits from scientific findings, we conduct our own basic research and participate in cooperative research projects. Our analytic and visualization methods have received multiple patents as well as the innovation award from the German Informatics Society (GI).</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-24 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title">
                    <span>26</span>
                    Information Design
                </div>
                <div class="body">
                    <p>DeltaMaster is a pioneer in visualization and design. Sparklines, graphic tables, and performance-based color coding are just a few examples of the visual stan­dards that DeltaMaster sets. We synchro­nize our layout, color, structures, and warning concepts with scientific laws of perception that apply to the brain, eyes, and ears - regardless of personal preferences and customs. This ensures that even new users quickly feel comfortable working with DeltaMaster reports.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-25 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title red">
                    <span>27</span>
                    Produkt design from Porsche Design Studio
                </div>
                <div class="body">
                    <p>We have perfected the reports and analyses in DeltaMaster in cooperation with the experts at Porsche Design Studio. The results are a high-resolution feast for your eyes. DeltaMaster always complements the available data and presents a dramatic, intuitive overview or precise detail.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-26 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title">
                    <span>28</span>
                    Prebuilt functionality
                </div>
                <div class="body">
                    <p>65 to 80 percent of project steps are identical in all companies. That's why we offer prebuilt functionality to simplify, unify, and automate these steps. Our ETL tools are designed for end users and even used by large clients.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-27 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title red">
                    <span>29</span>
                    Customer satisfaction
                </div>
                <div class="body">
                    <p>Studies such as the BI Survey from BARC regularly confirm that our clients are pleased with our product, service, and support. This is primarily due to the fact that our consultants answer many of our clients' questions directly. They aren't just fluent in BI, they are native speakers who offer their experience to tackle all your business, technical, and project-specific challenges. When you call our hotline, you are connected to our office - and not some call center.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-28 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title">
                    <span>30</span>
                    Consulting
                </div>
                <div class="body">
                    <p>If you need support, our excellent team of consultants can help you configure or even operate continual, automated data management - even from heterogeneous sources. We invest one day a week in training our consultants.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-29 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title red">
                    <span>31</span>
                    Partner
                </div>
                <div class="body">
                    <p>To support special application topics, we work with partners who are tightly integrated with our development and shine in their fields of expertise. The offerings of our partners range from pre-configured DeltaMaster solutions that support specific industries or processes to long-term operations.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-30 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title">
                    <span>32</span>
                    Awards and prizes
                </div>
                <div class="body">
                    <p>Our clients use DeltaMaster to gear up for the fierce competition in their mar&shy;kets. Some have won prizes for their innovative applications - for example, the Best Practice Award BI of the BARC Insti&shy;tute or the Service Management Prize the German Customer Service Association (KVD). The German Informatics Society (GI) has also honored us with their Inno&shy;vation Prize. DeltaMaster also receives top accolades from our users as well. In the annual BI Survey conducted by BARC, DeltaMaster regularly earns first place rankings in a variety of categories.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-31 center-block"></div>
                </div>
            </div>
        </div>
    </div>
        <div class="item highlight">
        <div class="inner">
            <div class="textbox">
                <div class="title red">
                    <span>33</span>
                    Early recognition and variance radars
                </div>
                <div class="body">
                    <p>You cannot put counteractions into effect overnight. That's why the analytic methods of DeltaMaster examine all changes in your organization down to their root causes. All reports contain variance signals that place each value in relation to your expectations, forecast, or budget. After all, you can only identify where you need to make changes, if they can work, or if you need to plan again from scratch if you have suitable comparisons. DeltaMaster tracks down the causes - even those buried deep in your data. This helps you see if specific product and customer attributes are the reasons behind your variances without losing sight of the big picture.</p>

                </div>
                <div class="footer text-center">
                    <div class="highlights-symbol highlights-symbol-32 center-block"></div>
                </div>
            </div>
        </div>
    </div>
    </div>
<div id="slick-pagination" class="slider-highlights">1/33</div>

            </div>
        </div>
    </section>

    <section id="section-facts" class="">
        <div id="block-facts" class="block block-1200 block-default">
            <h2>Factsheet</h2>
            <div class="content">

<div id="slider-facts" class="slider slick-default blue">
        <div class="item fact">
        <div class="inner">
            <div class="textbox">
                <div class="title">
                    Clients
                </div>
                <div class="body">
                    <ul>
	<li>Windows</li>
	<li>Web (IIS)</li>
	<li>iPad/iPhone</li>
	<li>Office Add in</li>
</ul>

                </div>
            </div>
        </div>
    </div>
        <div class="item fact">
        <div class="inner">
            <div class="textbox">
                <div class="title">
                    Databases
                </div>
                <div class="body">
                    <ul>
	<li>Microsoft SQL Server Analysis Services</li>
	<li>SAP BW</li>
	<li>SAP HANA</li>
	<li>Infor</li>
	<li>IBM Cognos TM1</li>
	<li>Oracle OLAP</li>
	<li>Oracle Essbase</li>
	<li>relational Database Systems</li>
</ul>

                </div>
            </div>
        </div>
    </div>
        <div class="item fact">
        <div class="inner">
            <div class="textbox">
                <div class="title">
                    Interfaces
                </div>
                <div class="body">
                    <ul>
	<li>XMLA/ADOMD.NET</li>
	<li>SAP OLAP BAPI</li>
	<li>ODBO</li>
	<li>OCI</li>
	<li>OLE DB</li>
	<li>ODBC</li>
	<li>ODP.NET</li>
	<li>XLS and MDB files</li>
</ul>

                </div>
            </div>
        </div>
    </div>
        <div class="item fact">
        <div class="inner">
            <div class="textbox">
                <div class="title">
                    Source systems
                </div>
                <div class="body">
                    <p>any, e. g.:</p>

<ul>
	<li>SAP ERP</li>
	<li>SAP R/3</li>
	<li>Microsoft Dynamics NAV</li>
	<li>Oracle</li>
	<li>Sage</li>
	<li>Infor</li>
</ul>

                </div>
            </div>
        </div>
    </div>
        <div class="item fact">
        <div class="inner">
            <div class="textbox">
                <div class="title">
                    Export formats
                </div>
                <div class="body">
                    <ul>
	<li>DeltaMaster report folders</li>
	<li>Microsoft Office (Word, Excel, PowerPoint, Outlook)</li>
	<li>PDF</li>
	<li>HTML</li>
	<li>CSV</li>
</ul>

                </div>
            </div>
        </div>
    </div>
        <div class="item fact">
        <div class="inner">
            <div class="textbox">
                <div class="title">
                    Analysis methods
                </div>
                <div class="body">
                    <ul>
	<li>Business methods</li>
	<li>Statistic methods</li>
	<li>Data Mining</li>
</ul>

                </div>
            </div>
        </div>
    </div>
    </div>
<div id="slick-pagination" class="slider-facts">1/6</div>

            </div>
        </div>
    </section>

    <section id="section-events" class="blue">
        <div id="block-events-list" class="block block-1200">

            <h2>Learn about DeltaMaster</h2>

            <div class="content">


<div id="slider-events" class="slider slick-default">
            <div class="item event ">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                November the 22nd, 16:00 - 17:30 CET            </div>
            <div class="city">
                                    Madrid
                            </div>
            <div class="title">
                                    <a href="https://www.bissantz.com/events/280"
                       data-src="https://www.bissantz.com/events/280?overlay=1"
                       data-path="280" data-title=""
                       class="ol-link ellipsis">
                        Presupuestación con DeltaMaster - integrado, intuitivo, inteligente
                    </a>
                            </div>
            <div class="details">
                                    <a href="https://www.bissantz.com/events/280"
                       data-src="https://www.bissantz.com/events/280?overlay=1"
                       data-path="280" data-title="Presupuestacion_con_DeltaMaster_-_integrado_intuitivo_inteligente-Madrid"
                       class="ol-link">
                                                    Details &gt;
                                            </a>
                            </div>
        </div>
        <div class="buttons">
                                                                        <a data-src="https://www.bissantz.com/events/280/subscribe"
                           class="ol-link ol-link-event-subscribe button button-red-white button-small">
                            Register                        </a>
                                                        </div>
    </div>
</div>            <div class="item event ">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                November the 22nd, 16:00 - 17:30 CET            </div>
            <div class="city">
                                    Barcelona
                            </div>
            <div class="title">
                                    <a href="https://www.bissantz.com/events/189"
                       data-src="https://www.bissantz.com/events/189?overlay=1"
                       data-path="189" data-title=""
                       class="ol-link ellipsis">
                        Presupuestación con DeltaMaster - integrado, intuitivo, inteligente
                    </a>
                            </div>
            <div class="details">
                                    <a href="https://www.bissantz.com/events/189"
                       data-src="https://www.bissantz.com/events/189?overlay=1"
                       data-path="189" data-title="Presupuestacion_con_DeltaMaster_-_integrado_intuitivo_inteligente-Barcelona"
                       class="ol-link">
                                                    Details &gt;
                                            </a>
                            </div>
        </div>
        <div class="buttons">
                                                                        <a data-src="https://www.bissantz.com/events/189/subscribe"
                           class="ol-link ol-link-event-subscribe button button-red-white button-small">
                            Register                        </a>
                                                        </div>
    </div>
</div>            <div class="item event ">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                November the 22nd, 16:00 - 17:30 CET            </div>
            <div class="city">
                                    Webinar
                            </div>
            <div class="title">
                                    <a href="https://www.bissantz.com/events/206"
                       data-src="https://www.bissantz.com/events/206?overlay=1"
                       data-path="206" data-title=""
                       class="ol-link ellipsis">
                        Presupuestación con DeltaMaster -  integrado, intuitivo, inteligente
                    </a>
                            </div>
            <div class="details">
                                    <a href="https://www.bissantz.com/events/206"
                       data-src="https://www.bissantz.com/events/206?overlay=1"
                       data-path="206" data-title="Presupuestacion_con_DeltaMaster_-__integrado_intuitivo_inteligente"
                       class="ol-link">
                                                    Details &gt;
                                            </a>
                            </div>
        </div>
        <div class="buttons">
                                                                        <a href="https://attendee.gotowebinar.com/register/5291216468484345859" target="_blank"
                           class="link-extern button button-red-white button-small">
                            Register                        </a>
                                                        </div>
    </div>
</div>            <div class="item event ">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                November the 28th, 11:00 - 12:00 CET            </div>
            <div class="city">
                                    Webinar
                            </div>
            <div class="title">
                                    Aufwertung der BWA mit DeltaMaster (Partnerwebinar von Prodato)
                            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item event ">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                November the 29th, 11:00 - 12:00 CET            </div>
            <div class="city">
                                    Webinar
                            </div>
            <div class="title">
                                    Planung mit DeltaMaster
                            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item event ">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                November the 29th, 08:30 - 11:00 CET            </div>
            <div class="city">
                                    Bonn
                            </div>
            <div class="title">
                                    Business Breakfast
                            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item event ">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                November the 30th, 10:00 - 16:00 CET            </div>
            <div class="city">
                                    Nürnberg
                            </div>
            <div class="title">
                                    Reporting und Kiosksysteme
                            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item event ">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                December the 6th, 11:00 - 11:30 CET            </div>
            <div class="city">
                                    Webinar
                            </div>
            <div class="title">
                                    Reporting mit DeltaMaster
                            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item event ">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                December the 13th, 11:00 - 11:30 CET            </div>
            <div class="city">
                                    Webinar
                            </div>
            <div class="title">
                                    Partner up to date
                            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item event ">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                December the 14th, 11:00 - 11:30 CET            </div>
            <div class="city">
                                    Webinar
                            </div>
            <div class="title">
                                    DeltaMaster in der Cloud
                            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item event ">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                January the 25th, 10:00 - 16:00 CET            </div>
            <div class="city">
                                    Nürnberg
                            </div>
            <div class="title">
                                    Reporting und Kiosksysteme
                            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item event ">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                March the 15th, 10:00 - 16:00 CET            </div>
            <div class="city">
                                    Nürnberg
                            </div>
            <div class="title">
                                    Reporting und Kiosksysteme
                            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item event ">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                May the 14th, 10:00 - 16:00 CET            </div>
            <div class="city">
                                    Nürnberg
                            </div>
            <div class="title">
                                    Reporting und Kiosksysteme
                            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item event ">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                May the 17th            </div>
            <div class="city">
                                    Berlin
                            </div>
            <div class="title">
                                    Bissantz Executive-Forum 2018
                            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item event ">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                July the 26th, 10:00 - 16:00 CET            </div>
            <div class="city">
                                    Nürnberg
                            </div>
            <div class="title">
                                    Reporting und Kiosksysteme
                            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>    </div>
<div id="slick-pagination" class="slider-events">1/15</div>

<div id="events-hidden" style="display:none;">
            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                                    <a href="https://www.bissantz.com/events/113"
                       data-src="https://www.bissantz.com/events/113?overlay=1"
                       data-path="113" data-title=""
                       class="ol-link ellipsis">
                        Reporting con DeltaMaster - integrado, intuitivo, inteligente
                    </a>
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                                    <a href="https://www.bissantz.com/events/142"
                       data-src="https://www.bissantz.com/events/142?overlay=1"
                       data-path="142" data-title=""
                       class="ol-link ellipsis">
                        Self service BI al alcance de todos con DeltaMaster: De un Excel a un informe representativo en pocos minutos
                    </a>
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                                    <a href="https://www.bissantz.com/events/145"
                       data-src="https://www.bissantz.com/events/145?overlay=1"
                       data-path="145" data-title=""
                       class="ol-link ellipsis">
                        Self service BI al alcance de todos con DeltaMaster: De un Excel a un informe representativo en pocos minutos
                    </a>
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                                    <a href="https://www.bissantz.com/events/147"
                       data-src="https://www.bissantz.com/events/147?overlay=1"
                       data-path="147" data-title=""
                       class="ol-link ellipsis">
                        Reporting con DeltaMaster - integrado, intuitivo, inteligente
                    </a>
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                                    <a href="https://www.bissantz.com/events/166"
                       data-src="https://www.bissantz.com/events/166?overlay=1"
                       data-path="166" data-title=""
                       class="ol-link ellipsis">
                        Reporting (in English)
                    </a>
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                                    <a href="https://www.bissantz.com/events/167"
                       data-src="https://www.bissantz.com/events/167?overlay=1"
                       data-path="167" data-title=""
                       class="ol-link ellipsis">
                        Reporting (in English)
                    </a>
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                                    <a href="https://www.bissantz.com/events/188"
                       data-src="https://www.bissantz.com/events/188?overlay=1"
                       data-path="188" data-title=""
                       class="ol-link ellipsis">
                        Self service BI al alcance de todos con DeltaMaster: De un Excel a un informe representativo en pocos minutos
                    </a>
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                                    <a href="https://www.bissantz.com/events/190"
                       data-src="https://www.bissantz.com/events/190?overlay=1"
                       data-path="190" data-title=""
                       class="ol-link ellipsis">
                        Self service BI al alcance de todos con DeltaMaster: De un Excel a un informe representativo en pocos minutos
                    </a>
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                                    <a href="https://www.bissantz.com/events/198"
                       data-src="https://www.bissantz.com/events/198?overlay=1"
                       data-path="198" data-title=""
                       class="ol-link ellipsis">
                        Planning with DeltaMaster (In English)
                    </a>
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                                    <a href="https://www.bissantz.com/events/204"
                       data-src="https://www.bissantz.com/events/204?overlay=1"
                       data-path="204" data-title=""
                       class="ol-link ellipsis">
                        Presupuestación con DeltaMaster -  integrado, intuitivo, inteligente
                    </a>
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                                    <a href="https://www.bissantz.com/events/205"
                       data-src="https://www.bissantz.com/events/205?overlay=1"
                       data-path="205" data-title=""
                       class="ol-link ellipsis">
                        Reporting con DeltaMaster - integrado, intuitivo, inteligente
                    </a>
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                                    <a href="https://www.bissantz.com/events/258"
                       data-src="https://www.bissantz.com/events/258?overlay=1"
                       data-path="258" data-title=""
                       class="ol-link ellipsis">
                        Presupuestación con DeltaMaster - integrado, intuitivo, inteligente
                    </a>
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>            <div class="item event hidden">
    <div class="inner">
        <div class="textbox">
            <div class="title">
                            </div>
        </div>
    </div>
</div>    </div>

<div class="text-center">
    <a class="button button-large " target="_blank" href="https://www.bissantz.com/webinars">
        Watch recorded webinars</a>
</div>
        </div>
    </div>

    </section>

    <section id="section-kennenlernen" class="">
        <div id="block-livedemo" class="block block-1200">

            <h2>Learn more<br />
about DeltaMaster</h2>

            <div class="content">
            <div class="container">
    <div class="row first">
        <div class="col first col-xs-12 col-sm-3">
            <i class="fa fa-phone"></i>
            <div class="title">
                Schedule a live demo
            </div>
            <div>
                <a href="tel:+499119355360" class="button button-red button-small">+49 911 935536-0</a>
            </div>
        </div>
        <div class="col col-xs-12 col-sm-3">
            <i class="fa fa-envelope"></i>
            <div class="title">
                Request a live demo
            </div>
            <div>
                <a data-src="https://www.bissantz.com/livedemo/subscribe"
                   class="ol-link ol-link-livedemo button button-red button-small">
                    Request
                </a>
            </div>
        </div>
        <div class="col last col-xs-12 col-sm-3">
            <i class="fa fa-arrow-circle-o-down"></i>
            <div class="title">
                DeltaMaster brochure
            </div>
            <div>
                                <a href="/files/products/DeltaMaster_Reporting_en.pdf" class="button button-red button-small" target="_blank">
                    Download
                </a>
            </div>
        </div>
        <div class="col last col-xs-12 col-sm-3">
            <i class="fa fa-play-circle"></i>
            <div class="title">
                Recorded webinars
            </div>
            <div>
                <a href="https://www.bissantz.com/webinars" class="button button-red button-small" target="_blank">
                    Request
                </a>
            </div>
        </div>
    </div>
</div>
        </div>
    </div>

    </section>

    <section id="section-references" class="red">
        <div id="block-references" class="block block-1200 block-default">
            <h2>References<br />
from all industries</h2>
            <div class="content">

<div id="slider-references" class="slider slick-default fixHeight">
                        <div class="item reference-cat">
                <div class="inner">
                    <div class="textbox">
                        <div class="title">
                            Automotive
                        </div>
                        <div class="body">
                            <ul>
                                                                                                                                                                                                                        <li>
                                        <a href="/files/references/DeltaMaster%20Success%20Story%20Aebi%20Schmidt.pdf" target="_blank">                                            Aebi Schmidt
                                        </a>                                    </li>
                                                                                                                                            <li>
                                                                                    AMAG
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Bosch
                                                                            </li>
                                                                                                                                                                                                                        <li>
                                        <a href="/files/references/DeltaMaster%20Success%20Story%20Daimler.pdf" target="_blank">                                            Daimler
                                        </a>                                    </li>
                                                                                                                                                                                                                        <li>
                                        <a href="/files/references/DeltaMaster%20Success%20Story%20Motea.pdf" target="_blank">                                            Motea
                                        </a>                                    </li>
                                                                                                                                            <li>
                                                                                    MS Motorservice International
                                                                            </li>
                                                                                                                                                                                                                        <li>
                                        <a href="/files/references/DeltaMaster%20Success%20Story%20NormaGroup.pdf" target="_blank">                                            NORMA Group
                                        </a>                                    </li>
                                                                                                                                            <li>
                                                                                    Porsche
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Reiff
                                                                            </li>
                                                                                                                                                                                                                        <li>
                                        <a href="/files/references/DeltaMaster%20Success%20Story%20Scherdel.pdf" target="_blank">                                            SCHERDEL
                                        </a>                                    </li>
                                                                                                                                            <li>
                                                                                    SMR Korea Automotive
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Volkswagen
                                                                            </li>
                                                            </ul>
                        </div>
                    </div>
                </div>
            </div>
                                <div class="item reference-cat">
                <div class="inner">
                    <div class="textbox">
                        <div class="title">
                            Beverages
                        </div>
                        <div class="body">
                            <ul>
                                                                                                                                            <li>
                                                                                    Brauerei Erdinger
                                                                            </li>
                                                                                                                                            <li>
                                                                                    MARS Drinks
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Mövenpick Wein
                                                                            </li>
                                                                                                                                            <li>
                                                                                    St. Jakobskellerei Schuler
                                                                            </li>
                                                            </ul>
                        </div>
                    </div>
                </div>
            </div>
                                <div class="item reference-cat">
                <div class="inner">
                    <div class="textbox">
                        <div class="title">
                            Chemical Industry
                        </div>
                        <div class="body">
                            <ul>
                                                                                                                                            <li>
                                                                                    Bayer Crop­Science
                                                                            </li>
                                                                                                                                            <li>
                                                                                    BrüggemannChemical
                                                                            </li>
                                                            </ul>
                        </div>
                    </div>
                </div>
            </div>
                                <div class="item reference-cat">
                <div class="inner">
                    <div class="textbox">
                        <div class="title">
                            Construction, DIY
                        </div>
                        <div class="body">
                            <ul>
                                                                                                                                            <li>
                                                                                    ABUS
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Conmetall Meister
                                                                            </li>
                                                                                                                                            <li>
                                                                                    DYWIDAG
                                                                            </li>
                                                                                                                                                                                                                        <li>
                                        <a href="/files/references/bissantz_ref_GustavAlberts_en.pdf" target="_blank">                                            Gustav Alberts
                                        </a>                                    </li>
                                                                                                                                            <li>
                                                                                    HEWI
                                                                            </li>
                                                                                                                                            <li>
                                                                                    MEA
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Sanitop-Wingenroth
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Schulz Farben
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Steinbacher Dämmstoff
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Storch-Ciret
                                                                            </li>
                                                                                                                                                                                                                        <li>
                                        <a href="/files/references/DeltaMaster%20Success%20Story%20Velux.pdf" target="_blank">                                            VELUX
                                        </a>                                    </li>
                                                                                                                                            <li>
                                                                                    wolfcraft
                                                                            </li>
                                                                                                                                                                                                                        <li>
                                        <a href="/files/references/DeltaMaster%20Success%20Story%20W%C3%BCrth.pdf" target="_blank">                                            Würth International
                                        </a>                                    </li>
                                                            </ul>
                        </div>
                    </div>
                </div>
            </div>
                                <div class="item reference-cat">
                <div class="inner">
                    <div class="textbox">
                        <div class="title">
                            Consumer Goods
                        </div>
                        <div class="body">
                            <ul>
                                                                                                                                            <li>
                                                                                    Essanelle
                                                                            </li>
                                                                                                                                            <li>
                                                                                    JUST Schweiz
                                                                            </li>
                                                                                                                                            <li>
                                                                                    KMP PrintTechnik
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Papstar
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Procter &amp; Gamble
                                                                            </li>
                                                                                                                                                                                                                        <li>
                                        <a href="/files/references/DeltaMaster%20Success%20Story%20Wenco.pdf" target="_blank">                                            wenco
                                        </a>                                    </li>
                                                            </ul>
                        </div>
                    </div>
                </div>
            </div>
                                <div class="item reference-cat">
                <div class="inner">
                    <div class="textbox">
                        <div class="title">
                            Energy
                        </div>
                        <div class="body">
                            <ul>
                                                                                                                                            <li>
                                                                                    E-Werk Wels
                                                                            </li>
                                                                                                                                            <li>
                                                                                    E.ON Business Services
                                                                            </li>
                                                                                                                                            <li>
                                                                                    IBB Holding
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Kelag
                                                                            </li>
                                                                                                                                            <li>
                                                                                    SMA Solar Techno­logy
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Städtische Werke Schaffhausen
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Von Roll Holding
                                                                            </li>
                                                            </ul>
                        </div>
                    </div>
                </div>
            </div>
                                <div class="item reference-cat">
                <div class="inner">
                    <div class="textbox">
                        <div class="title">
                            Financial Services
                        </div>
                        <div class="body">
                            <ul>
                                                                                                                                            <li>
                                                                                    BayernInvest
                                                                            </li>
                                                                                                                                            <li>
                                                                                    BonusCard.ch
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Deutsche Vermögensberatung DVAG
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Europäisch-Iranische Handelsbank
                                                                            </li>
                                                                                                                                                                                                                        <li>
                                        <a href="/files/references/DeltaMaster-Reference%20Hansainvest_0.pdf" target="_blank">                                            HANSAINVEST Real Assets
                                        </a>                                    </li>
                                                                                                                                            <li>
                                                                                    Leonteq Securities
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Pantaenius
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Porsche Financial Group
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Universal-Investment
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Velero Partners
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Verti Versicherungen
                                                                            </li>
                                                                                                                                            <li>
                                                                                    zfhn Zukunftsfonds Heilbronn
                                                                            </li>
                                                            </ul>
                        </div>
                    </div>
                </div>
            </div>
                                <div class="item reference-cat">
                <div class="inner">
                    <div class="textbox">
                        <div class="title">
                            Food
                        </div>
                        <div class="body">
                            <ul>
                                                                                                                                            <li>
                                                                                    BÜRGER
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Conditorei Coppenrath &amp; Wiese
                                                                            </li>
                                                                                                                                            <li>
                                                                                    ELO-FROST
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Glockenbrot Bäckerei
                                                                            </li>
                                                                                                                                            <li>
                                                                                    PMG Premium Mühlen Gruppe
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Schneekoppe
                                                                            </li>
                                                                                                                                                                                                                        <li>
                                        <a href="/files/references/DeltaMaster%20Success%20Story%20Schwartau_0.pdf" target="_blank">                                            Schwartauer Werke
                                        </a>                                    </li>
                                                                                                                                            <li>
                                                                                    SENNA Nahrungsmittel
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Wilhelm Brandenburg
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Wolf ButterBack
                                                                            </li>
                                                            </ul>
                        </div>
                    </div>
                </div>
            </div>
                                <div class="item reference-cat">
                <div class="inner">
                    <div class="textbox">
                        <div class="title">
                            Healthcare
                        </div>
                        <div class="body">
                            <ul>
                                                                                                                                            <li>
                                                                                    Diakonie-Klinikum Stuttgart
                                                                            </li>
                                                                                                                                            <li>
                                                                                    DRK gemeinnützige Krankenhausgesellschaft
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Franziskus Krankenhaus Linz
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Klinikum Altmühlfranken
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Klinikum Magdeburg
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Paul Gerhardt Diakonie
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Zuger Kantonsspital
                                                                            </li>
                                                            </ul>
                        </div>
                    </div>
                </div>
            </div>
                                <div class="item reference-cat">
                <div class="inner">
                    <div class="textbox">
                        <div class="title">
                            IT
                        </div>
                        <div class="body">
                            <ul>
                                                                                                                                            <li>
                                                                                    Bechtle
                                                                            </li>
                                                                                                                                            <li>
                                                                                    BISON IT Services
                                                                            </li>
                                                                                                                                            <li>
                                                                                    DATEV
                                                                            </li>
                                                            </ul>
                        </div>
                    </div>
                </div>
            </div>
                                <div class="item reference-cat">
                <div class="inner">
                    <div class="textbox">
                        <div class="title">
                            Industrial
                        </div>
                        <div class="body">
                            <ul>
                                                                                                                                                                                                                        <li>
                                        <a href="/files/references/DeltaMaster%20Success%20Story%20BJB_0.pdf" target="_blank">                                            BJB
                                        </a>                                    </li>
                                                                                                                                            <li>
                                                                                    Eagle­Burgmann
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Joseph Vögele
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Leica Camera
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Liebherr
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Mevaco
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Schreiner Group
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Spectro
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Voith
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Werner &amp; Pfleiderer
                                                                            </li>
                                                            </ul>
                        </div>
                    </div>
                </div>
            </div>
                                <div class="item reference-cat">
                <div class="inner">
                    <div class="textbox">
                        <div class="title">
                            Logistics
                        </div>
                        <div class="body">
                            <ul>
                                                                                                                                            <li>
                                                                                    bayernhafen Gruppe
                                                                            </li>
                                                                                                                                                                                                                        <li>
                                        <a href="/files/references/DeltaMaster%20Success%20Story%20CityJet.pdf" target="_blank">                                            CityJet
                                        </a>                                    </li>
                                                                                                                                            <li>
                                                                                    DB Cargo Logistics
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Friedrich Zufall
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Georg Fischer
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Germania
                                                                            </li>
                                                                                                                                            <li>
                                                                                    GOL
                                                                            </li>
                                                                                                                                            <li>
                                                                                    LSU Schäberle
                                                                            </li>
                                                                                                                                            <li>
                                                                                    People&#039;s Air Group
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Rhenus Logistics
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Röhlig
                                                                            </li>
                                                                                                                                            <li>
                                                                                    TFG Transfracht
                                                                            </li>
                                                            </ul>
                        </div>
                    </div>
                </div>
            </div>
                                <div class="item reference-cat">
                <div class="inner">
                    <div class="textbox">
                        <div class="title">
                            Medical Laboratories
                        </div>
                        <div class="body">
                            <ul>
                                                                                                                                            <li>
                                                                                    amedes Holding
                                                                            </li>
                                                                                                                                            <li>
                                                                                    ISG Intermed Service
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Labor 28
                                                                            </li>
                                                                                                                                            <li>
                                                                                    LADR Der Laborverbund
                                                                            </li>
                                                                                                                                            <li>
                                                                                    SYNLAB International
                                                                            </li>
                                                            </ul>
                        </div>
                    </div>
                </div>
            </div>
                                <div class="item reference-cat">
                <div class="inner">
                    <div class="textbox">
                        <div class="title">
                            Newspapers and Publishing Houses
                        </div>
                        <div class="body">
                            <ul>
                                                                                                                                                                                                                        <li>
                                        <a href="/files/references/DeltaMaster%20Success%20Story%20BauerMediaGroup.pdf" target="_blank">                                            Bauer Media Group
                                        </a>                                    </li>
                                                                                                                                            <li>
                                                                                    Die Zeit
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Mediengruppe Oberfranken
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Rheinische Post
                                                                            </li>
                                                                                                                                            <li>
                                                                                    rtv media group
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Südkurier
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Verlag Der Tagesspiegel
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Verlag SKV
                                                                            </li>
                                                            </ul>
                        </div>
                    </div>
                </div>
            </div>
                                <div class="item reference-cat">
                <div class="inner">
                    <div class="textbox">
                        <div class="title">
                            Pharmaceuticals
                        </div>
                        <div class="body">
                            <ul>
                                                                                                                                            <li>
                                                                                    Bayer HealthCare
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Boehringer Ingelheim
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Dr. August Wolff
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Ewopharma
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Grünenthal
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Henry Schein
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Novartis
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Pascoe
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Recordati Pharma
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Takeda
                                                                            </li>
                                                            </ul>
                        </div>
                    </div>
                </div>
            </div>
                                <div class="item reference-cat">
                <div class="inner">
                    <div class="textbox">
                        <div class="title">
                            Public Authorities
                        </div>
                        <div class="body">
                            <ul>
                                                                                                                                            <li>
                                                                                    Bau-, Verkehrs- und Energiedirektion des Kanton Bern
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Berliner Verkehrsbetriebe BVG
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Deutsche Gesellschaft für Qualität
                                                                            </li>
                                                                                                                                            <li>
                                                                                    German Society for International Cooperation (GIZ)
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Swiss Atom Surveillance ENSI
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Provincial Government of Vorarlberg
                                                                            </li>
                                                                                                                                                                                                                        <li>
                                        <a href="/files/references/DeltaMaster%20Success%20Story%20Verkehrsverbund%20Luzern_0.pdf" target="_blank">                                            Verkehsverbund Luzern
                                        </a>                                    </li>
                                                            </ul>
                        </div>
                    </div>
                </div>
            </div>
                                <div class="item reference-cat">
                <div class="inner">
                    <div class="textbox">
                        <div class="title">
                            Raw Materials
                        </div>
                        <div class="body">
                            <ul>
                                                                                                                                            <li>
                                                                                    Aluminium Oxid Stade
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Floragard
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Kisling
                                                                            </li>
                                                                                                                                                                                                                        <li>
                                        <a href="/files/references/DeltaMaster%20Success%20Story%20SalinenAustria.pdf" target="_blank">                                            Salinen Austria
                                        </a>                                    </li>
                                                            </ul>
                        </div>
                    </div>
                </div>
            </div>
                                <div class="item reference-cat">
                <div class="inner">
                    <div class="textbox">
                        <div class="title">
                            Real Estate
                        </div>
                        <div class="body">
                            <ul>
                                                                                                                                            <li>
                                                                                    BBT
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Berlinovo Immobilien
                                                                            </li>
                                                                                                                                            <li>
                                                                                    BIM Berliner Immobilienmanagement
                                                                            </li>
                                                                                                                                            <li>
                                                                                    BKW ISP
                                                                            </li>
                                                                                                                                            <li>
                                                                                    PRIVERA
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Siemens Real Estate
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Wohnbau Prenzlau
                                                                            </li>
                                                            </ul>
                        </div>
                    </div>
                </div>
            </div>
                                <div class="item reference-cat">
                <div class="inner">
                    <div class="textbox">
                        <div class="title">
                            Recycling
                        </div>
                        <div class="body">
                            <ul>
                                                                                                                                            <li>
                                                                                    Meinhardt Städtereinigung
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Stiftung GRS Batterien
                                                                            </li>
                                                            </ul>
                        </div>
                    </div>
                </div>
            </div>
                                <div class="item reference-cat">
                <div class="inner">
                    <div class="textbox">
                        <div class="title">
                            Retail
                        </div>
                        <div class="body">
                            <ul>
                                                                                                                                                                                                                        <li>
                                        <a href="/files/references/DeltaMaster%20Success%20Story%20Bucherer.pdf" target="_blank">                                            Bucherer
                                        </a>                                    </li>
                                                                                                                                            <li>
                                                                                    Comercial Mexicana
                                                                            </li>
                                                                                                                                            <li>
                                                                                    EDEKA Südwest
                                                                            </li>
                                                                                                                                                                                                                        <li>
                                        <a href="/files/references/DeltaMaster%20Success%20Story%20Kaiser%2BKraft_0.pdf" target="_blank">                                            Kaiser+Kraft
                                        </a>                                    </li>
                                                                                                                                            <li>
                                                                                    Media Saturn
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Sanitätshaus Seeger
                                                                            </li>
                                                                                                                                            <li>
                                                                                    ScSPORTS
                                                                            </li>
                                                                                                                                            <li>
                                                                                    SEARS Roebuck
                                                                            </li>
                                                                                                                                            <li>
                                                                                    SFC KOENIG
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Soriana
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Unternehmensgruppe Dr. Eckert
                                                                            </li>
                                                            </ul>
                        </div>
                    </div>
                </div>
            </div>
                                <div class="item reference-cat">
                <div class="inner">
                    <div class="textbox">
                        <div class="title">
                            Services
                        </div>
                        <div class="body">
                            <ul>
                                                                                                                                            <li>
                                                                                    ADAC
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Allianz Handwerker Services
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Dierkes Partner
                                                                            </li>
                                                                                                                                            <li>
                                                                                    diwa Personalservice
                                                                            </li>
                                                                                                                                            <li>
                                                                                    GKK Partners
                                                                            </li>
                                                                                                                                            <li>
                                                                                    JOB AG
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Neuapostolische Kirche International
                                                                            </li>
                                                                                                                                            <li>
                                                                                    OptiMedis
                                                                            </li>
                                                                                                                                            <li>
                                                                                    SOS-Kinderdorf
                                                                            </li>
                                                            </ul>
                        </div>
                    </div>
                </div>
            </div>
                                <div class="item reference-cat">
                <div class="inner">
                    <div class="textbox">
                        <div class="title">
                            Telecommunications
                        </div>
                        <div class="body">
                            <ul>
                                                                                                                                            <li>
                                                                                    Telefónica
                                                                            </li>
                                                            </ul>
                        </div>
                    </div>
                </div>
            </div>
                                <div class="item reference-cat">
                <div class="inner">
                    <div class="textbox">
                        <div class="title">
                            Textiles
                        </div>
                        <div class="body">
                            <ul>
                                                                                                                                            <li>
                                                                                    Deuter Sport
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Gudrun Sjödén
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Heinrich Heine Versand
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Mountain Force
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Popken Fashion
                                                                            </li>
                                                                                                                                            <li>
                                                                                    TRIGEMA
                                                                            </li>
                                                                                                                                            <li>
                                                                                    Wöhrl
                                                                            </li>
                                                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            </div>
<div id="slick-pagination" class="slider-references">1/23</div>

            </div>
        </div>
    </section>

    <section id="section-zitat4" class="quote">
        <div class="quote-inner">
    <div class="text">„With our DeltaMaster solution, we can analyze our company&#039;s entire subscription marketing.“</div>
    <div class="from">Ralf Vollert, Bauer Media Group</div>
</div>
    </section>

    <section id="section-publications" class="blue">
        <div id="block-newsletter" class="block block-1200">

            <h2>DeltaMaster clicks!<br />
our newsletter.</h2>

            <div class="content">
            <div class="container">
    <div class="row first">
        <div class="col left col-xs-12 col-sm-6">
            <form id="newsletter-form" action="https://www.bissantz.com/newsletter/subscribe/submit" method="post">
                <input type="hidden" name="newsletter[ts]" value="UH13SWdzBRp/AQ==" />
                <div class="headline">
                    Subscribe now
                </div>
                <div class="copytext">
                    ... and get DeltaMaster&nbsp;clicks! by e-mail every month.
                </div>
                <div class="icon-input input-blue-light">
                    <i class="fa fa-user"></i>
                    <input type="text" name="newsletter[name]" placeholder="NAME*" required="required">
                </div>
                <div class="icon-input input-blue-light">
                    <i class="fa fa-home"></i>
                    <input type="text" name="newsletter[company]" placeholder="COMPANY">
                </div>
                <div class="icon-input input-blue-light">
                    <i class="fa fa-envelope"></i>
                    <input type="email" name="newsletter[email]" placeholder="E-MAIL*" required="required">
                </div>
                <div class="">
                    <input type="text" name="newsletter[hp]" placeholder="HP" class="hp" />
                </div>
                <div>
                    <button type="submit" name="newsletter[submit]" value="1" class="button button-red-white button-with-loader">
                        Subscribe
                        <div class="loader"></div>
                    </button>
                    <div class="result"></div>
                </div>
            </form>
        </div>
        <div class="col right col-xs-12 col-sm-6">
            <div class="headline">
                The August issue:
            </div>
            <div class="copytext">
                Adapting the filter context – selecting what can be selected
            </div>
            <div class="icon text-center">
                <img class="pdf-icon" src="/themes/bissantz/images/icon-pdf.png" alt="Icon PDF" />
            </div>
            <div class="">
                <a href="https://www.bissantz.com/files/clicks/DeltaMaster_clicks_2017-08_EN_Filter_Context.pdf" class="button button-red-white" target="_blank">Current issue</a>
            </div>
            <div class="">
                <a target="_blank" href="http://clicks.bissantz.de/en/"
                   class="ol-link-nl-archiv button button-white ">
                    All issues                </a>
            </div>
        </div>
    </div>
</div>

        </div>
    </div>

    </section>

    <section id="section-blogs" class="blue">
        <div id="block-blogs" class="block">
            <h2>Our blogs</h2>
            <div class="content container">
                                <ul class="blog-list">
                                        <li class="blog-list-item">
                        <a href="http://forschung.bissantz.de/" target="_blank">
                            <div class="blog-list-item-inner top left red">
                                <div class="title">Bissantz Research</div>
                                <div class="subtitle">News from our labs<br/>(in German)</div>
                                <div class="footer"><br /></div>
                                <div class="corner hidden-xs"></div>
                            </div>
                        </a>
                    </li>
                                        <li class="blog-list-item">
                        <a href="http://crew.bissantz.de/" target="_blank">
                            <div class="blog-list-item-inner top right red">
                                <div class="title">Ready, steady, cube</div>
                                <div class="subtitle">We drive your data to the limit<br/>(in German)</div>
                                <div class="footer"><br /></div>
                                <div class="corner hidden-xs"></div>
                            </div>
                        </a>
                    </li>
                                        <li class="blog-list-item">
                        <a href="http://blog.bissantz.com/" target="_blank">
                            <div class="blog-list-item-inner bottom left white">
                                <div class="title">„Me, myself and BI“</div>
                                <div class="subtitle">Bissantz ponders</div>
                                <div class="footer"><br /></div>
                                <div class="corner hidden-xs"></div>
                            </div>
                        </a>
                    </li>
                                        <li class="blog-list-item">
                        <a href="http://www.bella-consults.com/" target="_blank">
                            <div class="blog-list-item-inner bottom right white">
                                <div class="title">Bella consults</div>
                                <div class="subtitle">Musings of the office dog at Bissantz</div>
                                <div class="footer"><br /></div>
                                <div class="corner hidden-xs"></div>
                            </div>
                        </a>
                    </li>
                                    </ul>
            </div>
        </div>
    </section>

    <section id="section-trainings" class="red">
        <div id="block-trainings-list" class="block block-1200">

            <h2>Trainings</h2>

                <div class="language-info">Training language is German, other languages on request.</div>
                <div class="content">


<div id="slider-trainings" class="slider slick-default">
            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                November the 28th, 09:00 - 17:00            </div>
            <div class="title">
                                    DeltaMaster-Individualschulung
                            </div>
            <div class="city">

            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                December the 5th, 09:00 - 17:00            </div>
            <div class="title">
                                    DeltaMaster II
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                December the 6th, 09:00 - 17:00            </div>
            <div class="title">
                                    DeltaMaster III
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                December the 12th, 09:00 - 17:00            </div>
            <div class="title">
                                    DeltaMaster I
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                December the 13th, 09:00 - 17:00            </div>
            <div class="title">
                                    OLAP
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                December the 13th, 09:00 - 17:00            </div>
            <div class="title">
                                    DeltaMaster-Individualschulung
                            </div>
            <div class="city">

            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                December the 15th, 09:00 - 17:00            </div>
            <div class="title">
                                    ETL
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                December the 19th, 09:00 - 17:00            </div>
            <div class="title">
                                    SQL
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                December the 20th, 09:00 - 17:00            </div>
            <div class="title">
                                    MDX
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                January the 15th, 09:00 - 17:00            </div>
            <div class="title">
                                    DeltaMaster I
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                January the 16th, 09:00 - 17:00            </div>
            <div class="title">
                                    OLAP
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                January the 18th, 09:00 - 17:00            </div>
            <div class="title">
                                    ETL
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                January the 23rd, 09:00 - 17:00            </div>
            <div class="title">
                                    DeltaMaster-Individualschulung
                            </div>
            <div class="city">

            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                February the 6th, 09:00 - 17:00            </div>
            <div class="title">
                                    DeltaMaster I
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                February the 7th, 09:00 - 17:00            </div>
            <div class="title">
                                    SQL
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                February the 8th, 09:00 - 17:00            </div>
            <div class="title">
                                    MDX
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                February the 20th, 09:00 - 17:00            </div>
            <div class="title">
                                    DeltaMaster II
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                February the 21st, 09:00 - 17:00            </div>
            <div class="title">
                                    DeltaMaster III
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                February the 27th, 09:00 - 17:00            </div>
            <div class="title">
                                    DeltaMaster-Individualschulung
                            </div>
            <div class="city">

            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                March the 5th, 09:00 - 17:00            </div>
            <div class="title">
                                    DeltaMaster I
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                March the 6th, 09:00 - 17:00            </div>
            <div class="title">
                                    OLAP
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                March the 8th, 09:00 - 17:00            </div>
            <div class="title">
                                    ETL
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                March the 13th, 09:00 - 17:00            </div>
            <div class="title">
                                    DeltaMaster-Individualschulung
                            </div>
            <div class="city">

            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                April the 11th, 09:00 - 17:00            </div>
            <div class="title">
                                    DeltaMaster-Individualschulung
                            </div>
            <div class="city">

            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                April the 16th, 09:00 - 17:00            </div>
            <div class="title">
                                    DeltaMaster II
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                April the 17th, 09:00 - 17:00            </div>
            <div class="title">
                                    DeltaMaster III
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                April the 23rd, 09:00 - 17:00            </div>
            <div class="title">
                                    DeltaMaster I
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                April the 24th, 09:00 - 17:00            </div>
            <div class="title">
                                    SQL
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                April the 25th, 09:00 - 17:00            </div>
            <div class="title">
                                    MDX
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                May the 2nd, 09:00 - 17:00            </div>
            <div class="title">
                                    OLAP
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                May the 4th, 09:00 - 17:00            </div>
            <div class="title">
                                    ETL
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                May the 8th, 09:00 - 17:00            </div>
            <div class="title">
                                    DeltaMaster-Individualschulung
                            </div>
            <div class="city">

            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                May the 15th, 09:00 - 17:00            </div>
            <div class="title">
                                    DeltaMaster I
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                June the 11th, 09:00 - 17:00            </div>
            <div class="title">
                                    DeltaMaster II
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                June the 12th, 09:00 - 17:00            </div>
            <div class="title">
                                    DeltaMaster III
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                June the 13th, 09:00 - 17:00            </div>
            <div class="title">
                                    DeltaMaster-Individualschulung
                            </div>
            <div class="city">

            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                June the 19th, 09:00 - 17:00            </div>
            <div class="title">
                                    DeltaMaster I
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                July the 3rd, 09:00 - 17:00            </div>
            <div class="title">
                                    SQL
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                July the 4th, 09:00 - 17:00            </div>
            <div class="title">
                                    MDX
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                July the 9th, 09:00 - 17:00            </div>
            <div class="title">
                                    DeltaMaster I
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                July the 10th, 09:00 - 17:00            </div>
            <div class="title">
                                    OLAP
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                July the 12th, 09:00 - 17:00            </div>
            <div class="title">
                                    ETL
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                July the 24th, 09:00 - 17:00            </div>
            <div class="title">
                                    DeltaMaster II
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                July the 25th, 09:00 - 17:00            </div>
            <div class="title">
                                    DeltaMaster III
                            </div>
            <div class="city">
                Nuremberg
            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>            <div class="item training">
    <div class="inner">
        <div class="textbox">
            <div class="date">
                                July the 31st, 09:00 - 17:00            </div>
            <div class="title">
                                    DeltaMaster-Individualschulung
                            </div>
            <div class="city">

            </div>
            <div class="details">
                                    &nbsp;
                            </div>
        </div>
        <div class="buttons">
                    </div>
    </div>
</div>    </div>
<div id="slick-pagination" class="slider-trainings">1/45</div>
        </div>
    </div>

    </section>

    <section id="section-contact" class="block">
        <div id="block-contacts" class="block block-1200">

            <h2>Contact</h2>

            <div class="content">
            <div class="tabs">
    <ul class="tab-links">
                <li class="active"><a data-href="#tab-1">Nürnberg</a></li>
                <li><a data-href="#tab-2">Hamburg</a></li>
                <li><a data-href="#tab-3">Darmstadt</a></li>
            </ul>

    <div class="tab-content container">
                <div id="tab-1" class="tab active"
             data-coords='{"lat":49.469209,"lng":11.0852}'>
            <div class="row first text-center">
                <div class="col first col-xs-12 col-sm-4">
                    <i class="fa fa-phone"></i>
                    <div class="text">+49 911 935536-0</div>
                </div>
                <div class="col second col-xs-12 col-sm-4">
                    <i class="fa fa-envelope"></i>
                    <div class="text"><a href="mailto:service@bissantz.de">service@bissantz.de</a></div>
                </div>
                <div class="col last col-xs-12 col-sm-4">
                    <i class="fa fa-home"></i>
                    <div class="text">
                        Bissantz &amp; Company GmbH<br />
                                                Nordring 98, 90409 Nürnberg
                    </div>
                </div>
            </div>
        </div>
                <div id="tab-2" class="tab "
             data-coords='{"lat":53.55215,"lng":10.00164}'>
            <div class="row first text-center">
                <div class="col first col-xs-12 col-sm-4">
                    <i class="fa fa-phone"></i>
                    <div class="text">+49 911 935536-0</div>
                </div>
                <div class="col second col-xs-12 col-sm-4">
                    <i class="fa fa-envelope"></i>
                    <div class="text"><a href="mailto:service@bissantz.de">service@bissantz.de</a></div>
                </div>
                <div class="col last col-xs-12 col-sm-4">
                    <i class="fa fa-home"></i>
                    <div class="text">
                        Bissantz &amp; Company GmbH<br />
                        Büro Hamburg<br />                        Lilienstraße 19, 20095 Hamburg
                    </div>
                </div>
            </div>
        </div>
                <div id="tab-3" class="tab "
             data-coords='{"lat":49.8717,"lng":8.63478}'>
            <div class="row first text-center">
                <div class="col first col-xs-12 col-sm-4">
                    <i class="fa fa-phone"></i>
                    <div class="text">+49 911 935536-0</div>
                </div>
                <div class="col second col-xs-12 col-sm-4">
                    <i class="fa fa-envelope"></i>
                    <div class="text"><a href="mailto:service@bissantz.de">service@bissantz.de</a></div>
                </div>
                <div class="col last col-xs-12 col-sm-4">
                    <i class="fa fa-home"></i>
                    <div class="text">
                        Bissantz &amp; Company GmbH<br />
                        Büro Darmstadt<br />                        Rheinstraße 97, 64295 Darmstadt
                    </div>
                </div>
            </div>
        </div>
            </div>
</div>

        </div>
    </div>

        <div class="map">
            <div id="map-canvas"></div>
        </div>
    </section>
    </div>

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "url": "https://www.bissantz.com/",
  "logo": "https://www.bissantz.com/themes/bissantz/images/logo.png",
  "contactPoint": [{
      "@type": "ContactPoint",
      "telephone": "+49 911 935536-700",
      "contactType": "technical support"
  }],
  "sameAs": [
    "https://www.facebook.com/BissantzCompanyGmbh",
    "https://www.twitter.com/bissantz",
    "https://www.xing.com/companies/bissantz%26companygmbh"
  ]
}
</script>


<footer id="footer">
    <div class="container">
        <div class="row text-center social-media-links">
            <a href="https://www.facebook.com/BissantzCompanyGmbh"            target="_blank"><i class="fa fa-facebook"    ></i></a>
            <a href="https://twitter.com/Bissantz"                            target="_blank"><i class="fa fa-twitter"     ></i></a>
            <a href="https://de.linkedin.com/company/bissantz-&-company-gmbh" target="_blank"><i class="fa fa-linkedin"    ></i></a>
            <a href="https://www.xing.com/companies/bissantz%26companygmbh"   target="_blank"><i class="fa fa-xing"        ></i></a>
            <a href="https://www.youtube.com/BissantzCompany"                 target="_blank"><i class="fa fa-youtube-play"></i></a>
        </div>
    </div>
    <div class="container">
        <div class="row first">
            <div class="col col-xs-6 col-sm-4 col-md-2"><a href="https://www.bissantz.com/products">DeltaMaster</a></div>
            <div class="col col-xs-6 col-sm-4 col-md-2"><a href="https://www.bissantz.com/#events">Meet</a></div>
            <div class="col col-xs-6 col-sm-4 col-md-2"><a href="https://www.bissantz.com/#references">Clients</a></div>
            <div class="col col-xs-6 col-sm-4 col-md-2"><a href="https://www.bissantz.com/#publications">Publications</a></div>
            <div class="col col-xs-6 col-sm-4 col-md-2"><a href="https://www.bissantz.com/#trainings">Trainings</a></div>
            <div class="col col-xs-6 col-sm-4 col-md-2"><a href="https://www.bissantz.com/#contact">Contact</a></div>
        </div>
        <div class="row second">
            <div class="col col-xs-6 col-sm-4 col-md-2"><a class="ol-link-support">Support</a></div>
            <div class="col col-xs-6 col-sm-4 col-md-2"><a class="ol-link-login" href="http://legacy.bissantz.com/login">Customer Portal</a></div>
            <div class="col col-xs-6 col-sm-4 col-md-2"><a class="ol-link-partner" href="http://legacy.bissantz.com/partner">Partner Portal</a></div>
            <div class="col col-xs-6 col-sm-4 col-md-2"><a href="https://www.bissantz.com/jobs">Jobs</a></div>
            <div class="col col-xs-6 col-sm-4 col-md-2"><a href="https://www.bissantz.com/about-us">About us</a></div>
            <div class="col col-xs-6 col-sm-4 col-md-2"><a class="ol-link-impressum">Imprint/<span class="nowrap">Privacy Notice</span></a></div>
        </div>
        <div class="row last text-center copyright">
            &copy; 2017 Bissantz &amp; Company GmbH. All rights reserved.
        </div>
    </div>
</footer>




<!-- Overlays -->

<div id="ol-support" class="overlay mfp-hide">
    <div class="ol-close-btn"></div>
    <div class="title">
    </div>
    <div class="title2">
        DeltaMaster Support Hotline
    </div>
    <div class="content refred">
        <p>
                                    To reach our support team, dial <a href="tel:+49911935536700">+49 911 935536-700</a> or send an e-mail to <a href="mailto:support@bissantz.de">support@bissantz.de</a>.
        </p>
        <div class="divider"></div>
    </div>

    <div class="title2">
        Citrix
    </div>
    <div class="divider"></div>
    <div class="content">
                                    <div class="divider">
            <div class="subtitle">Download:</div>
            <div class="twocols">
                <div class="onecol">
                    <span class="small">
                        <a href="https://download.citrixonline.com/manual.html?buildNumber=888&amp;startMode=Join&amp;productName=g2ax_customer&amp;programFile=g2ax_customer_downloadhelper_win32_x86.exe&amp;manualDownloadFile=g2ax_customer_combined_dll_full_win32_x86_en_US.exe&amp;startAsService=false&amp;serviceAllowed=true&amp;uninstallService=true&amp;locale=en_US&amp;theme=g2ax&amp;manualDownloadFileAlias=g2a_rs_customer_installer_en_US&amp;egwHostname=egw1.express.gotoassist.com&amp;egwPort=8200&amp;egwPort=80&amp;egwPort=443&amp;egwIp=216.115.218.197" target="_blank">
                            Citrix GoToAssist
                        </a>
                    </span>
                </div>
                <div class="onecol">
                    <span class="small">
                       <a href="http://joinwebinar.com/fec/?locale=en&amp;set=true" target=_blank">
                           Citrix GoToWebinar
                       </a>
                    </span>
                </div>
            </div>
            <div class="clr"></div>
        </div>
        <div class="divider">
            <div class="subtitle">Test:</div>
            <div class="twocols">
                <div class="onecol">
                    <span class="small">
                        <a href="http://support.citrixonline.com/en_US/gotoassist%20corporate/help_files/GTA130007?title=Connection+Wizard" target="_blank">
                            Connection Wizard GotoAssist
                        </a>
                    </span>
                </div>
                <div class="onecol">
                    <span class="small">
                        <a href="https://support.citrixonline.com/en_US/Meeting/all_files/G2M050001#Join" target="_blank">
                            Test page for GotoMeeting
                        </a>
                    </span>
                </div>
            </div>
        </div>
        <div class="clr"></div>
        <div class="divider"></div>
        <div class="divider">
            <div class="subtitle">WebStarter:</div>
            <iframe data-src="https://www.fastsupport.com/" height="286" width="100%" frameborder="0" scrolling="no"></iframe>
        </div>
    </div>
</div>


<div id="ol-impressum" class="overlay mfp-hide">
    <div class="ol-close-btn"></div>
    <div class="title">
        Imprint
    </div>
    <div class="title2">
        Bissantz & Company GmbH
    </div>
    <div class="content">
        <p>
            Nordring 98<br/>
            90409 Nuremberg<br/>
            Germany<br/>
            Phone: +49 911 935536-0<br/>
            Fax: +49 911 935536-10<br/>
            <br/>
            <a href="mailto:service@bissantz.de">service@bissantz.de</a><br/>
            <br/>
            Managing Directors:<br/>
            Dr. Nicolas Bissantz<br/>
            Michael Westphal<br/>
            <br/>
            Commercial Register:<br/>
            Nuremberg Municipal Court, HRB 15590<br/>
            <br/>
            VAT ID: DE181064258<br/>
            Tax number: 241/118/25048<br/>
            <br/>
            <br/>
            Responsible for content according to § 55 paragraph 2 RStV:<br/>
            <br/>
            Dr. Nicolas Bissantz<br/>
            Bissantz & Company GmbH<br/>
            Nordring 98<br/>
            90409 Nuremberg<br/>
            <br/>
            We are not willing or obliged to participate in dispute settlement procedures before a consumer arbitration board.<br/>
            <br/>
            <div class="dataprivacy"><h2>Data Privacy Policy</h2>

<p>Our website may be used without entering personal information. Different rules may apply to certain services on our site, however, and are explained separately below. We collect personal information from you (e.g. name, address, email address, telephone number, etc.) in accordance with the provisions of German data protection statutes. Information is considered personal if it can be associated exclusively to a specific natural person. The legal framework for data protection may be found in the German Federal Data Protection Act (BDSG) and the Telemedia Act (TMG). The provisions below serve to provide information as to the manner, extent and purpose for collecting, using and processing personal information by the provider.</p>

<p><strong>Bissantz & Company</strong></p>

<p><strong>+49 911 935536-0</strong></p>

<p><strong>service@bissantz.de</strong></p>

<p>Please be aware that data transfer via the internet is subject to security risks and, therefore, complete protection against third-party access to transferred data cannot be ensured.</p>

<h4>Cookies</h4>

<p>Our website makes use of so-called cookies in order to recognize repeat use of our website by the same user/internet connection subscriber. Cookies are small text files that your internet browser downloads and stores on your computer. They are used to improve our website and services. In most cases these are so-called "session cookies" that are deleted once you leave our website.</p>

<p>To an extent, however, these cookies also pass along information used to automatically recognize you. Recognition occurs through an IP address saved to the cookies. The information thereby obtained is used to improve our services and to expedite your access to the website.</p>

<p>You can prevent cookies from being installed by adjusting the settings on your browser software accordingly. You should be aware, however, that by doing so you may not be able to make full use of all the functions of our website.</p>

<h4>Server Data</h4>

<p>For technical reasons, data such as the following, which your internet browser transmits to us or to our web space provider (so called server log files), is collected:</p>

<ul>
	<li>type and version of the browser you use</li>
	<li>operating system</li>
	<li>websites that linked you to our site (referrer URL)</li>
	<li>websites that you visit</li>
	<li>date and time of your visit</li>
	<li>your Internet Protocol (IP) address.</li>
</ul>

<p>This anonymous data is stored separately from any personal information you may have provided, thereby making it impossible to connect it to any particular person. The data is used for statistical purposes in order to improve our website and services.</p>

<h4>Purpose of Registering</h4>

<p>We offer you the opportunity to sign up for our website. The information entered when signing up, as shown in the registration form is collected and stored solely for use by our website. When signing up for our website, we also store your IP address and the date and time you registered. This serves to protect us in the event a third party improperly and without your knowledge makes use of your data to sign up for our site.None of this information is transferred to third parties. Nor is any of this information matched to any information that may be collected by other components of our website.</p>

<h4>Newsletter</h4>

<p>Our website offers you the opportunity to subscribe to our newsletter. The newsletter provides you periodically with information about our services. To receive our newsletter, we require a valid email address. We will review the email address you provide for the purpose of determining whether you are in fact the owner of the email address provided or whether the actual owner of said address is authorized to receive the newsletter. When subscribing to our newsletter, we will store your IP address as well as the date and time you subscribed. This serves to protect us in the event a third party improperly and without your knowledge makes use of your email address to subscribe to our newsletter. We will not collect any other data. The data thereby collected is used solely for the purpose of receiving our newsletter. No data is transferred to third parties. Nor is any of this information matched to any information that other components of our website may collect. You may cancel your subscription to the newsletter at any time. You will find additional details in the email confirming your subscription as well as in each newsletter.</p>

<h4>Contacting Us</h4>

<p>On our website we offer you the opportunity to contact us, either by email and/or by using a contact form. In such event, information provided by the user is stored for the purpose of facilitating communications with the user. No data is transferred to third parties. Nor is any of this information matched to any information that may be collected by other components of our website.</p>

<h4>Posting Comments</h4>

<p>On our website we offer you the opportunity to post comments about individual articles. For this purpose, the IP address of the user/internet connection subscriber is stored. This information is stored for our security in the event the author through his/her comments infringes against third party rights and/or unlawful content is entered. Consequently, we have a direct interest in the author’s stored data, particularly since we may be potentially liable for such violations. No data is transferred to third parties. Nor is any of this information matched to any information that may be collected by other components of our website.</p>

<h4>Subscribing to Comments</h4>

<p>On our website we offer you the opportunity to subscribe to subsequent comments about an article which you intend to comment on. When you choose this option, you will receive a confirmation email which is used to determine if you are actually the owner of the email address entered. You may at any time revoke your decision to subscribe to such follow-on comments. You will find additional details in the confirmation email. No data hereby obtained is transferred to third parties. Nor is any of this information matched to any information that may be collected by other components of our website.</p>

<h4>Use of Google Analytics with anonymization</h4>

<p>Our website uses Google Analytics, a web analysis service from Google Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043 USA, hereinafter referred to as “Google“. Google Analytics employs so-called “cookies“, text files that are stored to your computer in order to facilitate an analysis of your use of the site.</p>

<p>The information generated by these cookies, such as time, place and frequency of your visits to our site, including your IP address, is transmitted to Google’s location in the US and stored there.</p>

<p>We use Google Analytics with an IP anonymization feature on our website. In doing so, Google abbreviates and thereby anonymizes your IP address before transferring it from member states of the European Union or signatory states to the Agreement on the European Economic Area.</p>

<p>Google will use this information to evaluate your usage of our site, to compile reports on website activity for us, and to provide other services related to website-&nbsp;and internet usage. Google may also transfer this information to third parties if this is required by law or to the extent this data is processed by third parties&nbsp;on Google´s behalf.</p>

<p>Google states that it will in never associate your IP address with other data held by Google. You can prevent cookies from being installed by adjusting the settings on your browser software accordingly. You should be aware, however, that by doing so you may not be able to make full use of all the functions of our website.</p>

<p>Google also offers a disabling option for the most common browsers, thus providing you with greater control over&nbsp;the data which is collected and processed by Google. If you enable this option, no information regarding your website visit is transmitted to Google Analytics. However, the activation does not prevent the transmission of information to us or to any other web analytics services we may use. For more information about the disabling option provided by Google, and how to enable this option, visit <a href="https://tools.google.com/dlpage/gaoptout?hl=de">https://tools.google.com/dlpage/gaoptout?hl=en</a></p>

<h4>Use of Google+ recommendation components</h4>

<p>Our website employs the “+1“-buttom from Google+ belonging to Google Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043 USA, hereinafter referred to as “Google“. Each time our website receives an access request equipped with a “+1“ component, the component prompts your browser to download an image of this component from Google. Through this process, Google is informed precisely which page of our website is being visited.&nbsp; As specified by Google, your visit undergoes additional analysis in the event you are not logged into your Google account.&nbsp; If you access our site while logged into Google and press the “+1" button, Google can collect information about your Google account, websites you recommend as well as your IP address, along with other browser-related information.&nbsp; This allows your “+1“ recommendation to be stored and publicized. Your Google “+1“ recommendation can then appear as a reference in other Google services, such as search results, your Google account or other places, such as on websites and ads in the internet, , together with your account name and, if applicable, a picture you provided to Google. Furthermore, Google can link your visit to our site with data stored by Google.&nbsp;Google also records this information for the purpose of further improving Google services. If you wish to minimize the collection of information by Google as previously described, you must log out of your Google account before visiting our website.</p>

<p>You can access Google‘s data protection policies relating to the “+1“ button together with all relevant information on the collection, transfer and use of data by Google, your rights in this regard as well as your profile settings options at the following link:</p>

<p>&nbsp;<a href="https://developers.google.com/+/web/buttons-policy">https://developers.google.com/+/web/buttons-policy</a></p>

<h4>Use of Facebook components</h4>

<p>Our website employs components provided by facebook.com. Facebook is a service of Facebook Inc., 1601 S. California Ave, Palo Alto, CA 94304, USA.</p>

<p>Each time our website receives an access request equipped with a Facebook component, the component prompts your browser to download an image of this Facebook component. Through this process, Facebook is informed precisely which page of our website is being visited.&nbsp;</p>

<p>When you access our site while logged into Facebook, Facebook uses information gathered by this component to identify the precise page you are viewing and associates this information to your personal account on Facebook. Whenever you click on the&nbsp;“Like“ button, for example, or enter a comment, this information is transmitted to your personal account on Facebook and stored there. In addition, Facebook is informed of your visit to our website. This occurs regardless of whether you click on a component or not.</p>

<p>If you wish to prevent the transfer to and storage of data by Facebook about you and your interaction with our website, you must first log out of Facebook before visiting our website. The data protection policies of Facebook provide additional information, in particular about the collection and use of data by Facebook, your rights in this regard as well as the options available to you for protecting your privacy: <a href="https://de-de.facebook.com/about/privacy/">https://de-de.facebook.com/about/privacy/</a></p>

<p>In addition, tools are freely available on the market that can be used to block Facebook social plug-ins with add-ons from being added to all commonly used browsers: <a href="http://webgraph.com/resources/facebookblocker/">http://webgraph.com/resources/facebookblocker/</a><br />
You can find an overview of Facebook plugins at <a href="https://developers.facebook.com/docs/plugins/">https://developers.facebook.com/docs/plugins/</a></p>

<h4>Use of Twitter recommendation components</h4>

<p>Our website employs components provided by Twitter. Twitter is a service of Twitter Inc., 795 Folsom St., Suite 600, San Francisco, CA 94107, USA.</p>

<p>Each time our website receives an access request equipped with a Twitter component, the component prompts your browser to download an image of this component from Twitter. Through this process, Twitter is informed precisely which page of our website is being visited. We have no control over the data that Twitter collects in the process, or over the extent of the data that Twitter collects. To the best of our knowledge, Twitter collects the URL of each website accessed as well as the IP address of the user, but does so solely for the purpose of displaying Twitter components. Additional information may be obtained from the Twitter data privacy policy, at: <a href="http://twitter.com/privacy">http://twitter.com/privacy</a>.</p>

<p>You may change your data privacy settings in your account settings, at <a href="http://twitter.com/account/settings">http://twitter.com/account/settings</a>.</p>

<h4>Use of Xing recommendation components</h4>

<p>Our website employs components provided by the network XING.com. These components are a service of XING AG, Dammtorstraße 29-32, 20354 Hamburg, Germany. Each time our website receives an access request equipped with a XING component, the component prompts your browser to download an image of this component from XING.</p>

<p>To the best of our knowledge, XING does not store any personal information from the user obtained through accessing our website. XING also does not store IP addresses. In addition, no analysis of user activity occurs through the use of cookies in connection with the “XING Share-Button”. Additional information may be found in the data privacy provisions relating to the XING Share-Button, at: <a href="https://www.xing.com/app/share?op=data_protection">https://www.xing.com/app/share?op=data_protection</a></p>

<h4>Use of LinkedIn recommendation components</h4>

<p>Our website employs components provided by the network LinkedIn. LinkedIn is a service of LinkedIn Corporation, 2029 Stierlin Court, Mountain View, CA 94043, USA. Each time our website receives an access request equipped with a LinkedIn component, the component prompts your browser to download an image of this component from LinkedIn. Through this process, LinkedIn is informed exactly which page of our website is being accessed. By clicking the LinkedIn “recommend button“ while logged into your LinkedIn account, you can link content from our website to your LinkedIn profile. This allows LinkedIn to associate your visit to our site with your LinkedIn account.</p>

<p>We have no control over the data that LinkedIn collects thereby, nor over the extent of the data that LinkedIn collects. Nor do we have any knowledge of the content of data transferred to LinkedIn. Details on data collection by LinkedIn as well as your rights in this regard and your browser setting options may be obtained from the LinkedIn data privacy policy, which may be accessed at: <a href="http://www.linkedin.com/legal/privacy-policy">http://www.linkedin.com/legal/privacy-policy</a></p>

<h4>Use of YouTube components with enhanced data protection mode</h4>

<p>On our website we use components (videos) of YouTube, LLC 901 Cherry Ave., 94066 San Bruno, CA, USA, a company belonging to Google Inc., Amphitheatre Parkway, Mountain View, CA 94043, USA.</p>

<p>To this end, we use the “ - enhanced data protection mode - ” option provided by YouTube.</p>

<p>When you display a page that has an embedded video, a connection will be made to the YouTube server and the content will appear on the website via a communication to your browser.</p>

<p>According to the information provided by YouTube, in “ - enhanced data protection mode -”, data is only transferred to the YouTube server, in particular which of our websites you have visited, if you watch the video. If you are logged onto YouTube at the same time, this information will be matched to your YouTube member account. You can prevent this from happening by logging out of your member account before visiting our website.</p>

<p>Further information about data protection by YouTube is provided by Google under the following link:</p>

<p><a href="https://www.google.de/intl/de/policies/privacy/">https://www.google.de/intl/de/policies/privacy/</a></p>

<h4>The use of Google Remarketing</h4>

<p>On our website, we promote the service Google Remarketing belonging to the company Google Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043 USA, hereinafter "Google". Advertisements may appear for users who have already visited our website in the past with Google Remarketing. Within the Google network, advertisements adapted to your interests can hereby be displayed on our site. Google Remarketing uses cookies for this evaluation. Cookies are small text files stored on your computer which allow the use of the website to be analyzed. This makes it possible to recognize our visitors if these sites are accessed within the advertising network of Google. In this way, advertisements can be presented within the advertising network of Google based on content which has previously been accessed on websites within the Google advertising network by visitors who are also using the Google Remarketing feature. Google does not itself collect any personal data. You can disable this feature by making the appropriate settings under <a href="http://www.google.com/settings/ads">http://www.google.com/settings/ads</a>.&nbsp;</p>

<h4>Use of Google-Adwords</h4>

<p>For purposes of promotion, our website also employs the Google ad tool "Google-Adwords". As part of this, our website employs the analysis service "Conversion-Tracking" from Google Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043 USA, hereinafter referred to as “Google“. If you access our site by way of a Google ad, a cookie is placed on your computer. Cookies are small text files that your internet browser downloads and stores to your computer. These so-called "conversion cookies" cease to be active after 30 days and are not used to identify you personally. If you visit certain pages of our website while the cookie is still active, we and Google know that you, as user, have clicked on ads placed on Google and were redirected to our site. Google uses the information obtained through "conversion cookies" to compile statistics for our website. These statistics tell us the total number of users who have clicked on our ad as well as which pages of our site were then accessed by each user. However, neither we nor other advertisers who use "Google-Adwords" receive any kind of information that can be used to personally identify users. You can prevent the installation of "conversion cookies" by making the appropriate change to your browser settings, for example by setting your browser so that the automatic placement of cookies is deactivated or by blocking cookies from the domain "googleadservices.com“.</p>

<p>You can obtain the relevant data privacy policy from Google at the following link:</p>

<p><a href="https://services.google.com/sitestats/de.html">https://services.google.com/sitestats/de.html</a></p>

<h3>Information/Cancellation/Deletion</h3>

<p>On the basis of the Federal Data Protection Act, you may contact us at no cost if you have questions relating to the collection, processing or use of your personal information, if you wish to request the correction, blocking or deletion of the same, or if you wish to cancel explicitly granted consent. Please note that you have the right to have incorrect data corrected or to have personal data deleted, where such claim is not barred by any legal obligation to retain this data.</p>

</div>
            <br/>
            <br/>
                        <a href="/files/docs/General_Terms_and_Conditions_-_Bissantz_and_Company.pdf" target="_blank">
                General Terms and Conditions of Contract (AGB)
            </a><br/>
            <br/>
                        <a href="/files/docs/General_Terms_and_Conditions_for_Events_-_Bissantz_and_Company.pdf" target="_blank">
                General Terms and Conditions for Events
            </a><br/>
        </p>
    </div>
</div>


<div id="ol-partner" class="overlay mfp-hide">
    <div class="ol-close-btn"></div>
    <div class="title">
    </div>
    <div class="title2">
        Partner Portal
    </div>
    <div class="content">
        <div class="divider"></div>
        <form target="_blank" action="https://legacy.bissantz.de/partner/index.asp" method="post" class="form" accept-charset="ISO-8859-1">
            <input type="hidden" name="submitted" value="true">
            <input type="hidden" name="lang" value="de">

            <div class="form-group">
                <p><label>Please log in:</label></p>
                <input class="input-small blue-border" type="text" name="loginname" placeholder="User name" required="required">
            </div>
            <div class="form-group">
                <input class="input-small blue-border" type="password" name="passwd" placeholder="Password" required="required">
            </div>
            <div class="form-group">
                <button type="submit" value="anmelden" class="button button-blue button-small-small">Let me in</button>
            </div>
            <div class="divider"></div>
            <div class="help">
                <small>
                    <a target="_blank" href="https://www.bissantz.com/contact/login_daten_nicht_zur_hand">
                        Can't remember your login details?
                    </a><br/>
                    Please make sure that your browser is configured to accept session cookies.
                </small>
            </div>
        </form>
        <div class="divider"></div>
    </div>
    <div class="content">
        <p>
            <strong>Our partner program</strong> is suitable for:
            <ul>
                <li>Management consultancies</li>
                <li>Systems integrators</li>
                <li>OEM and software providers</li>
                <li>Reseller</li>
                <li>Start-ups</li>
            </ul>
        </p>
        <p>
<strong>Sales strategy and market appearance</strong> will be prepared in cooperation with us.
Comprehensive trainings and pragmatic, professional support in the start-up phase ensure your success.        </p>
        <p>
            <strong>Learn more?</strong> Contact our partner management team:<br/>
            <a href="mailto:partner@bissantz.de">partner@bissantz.de</a><br/>
            <a href="tel:+49 911 935536-0">+49 911 935536-0</a>
        </p>
    </div>
</div>


<div id="ol-login" class="overlay mfp-hide">
    <div class="ol-close-btn"></div>
    <div class="title">
    </div>
    <div class="title2">
        Login
    </div>
    <div class="title3">
        Client and partner area
    </div>
    <div class="content">
        <strong>Hotline</strong><br/>
        for Customers and Partners:<br/>
        <a href="tel:+49911935536700">+49 911 935536-700</a>
    </div>
    <div class="content">
        <div class="divider"></div>
        <form target="_blank" action="https://legacy.bissantz.de/login/index.asp" method="post" class="form" accept-charset="ISO-8859-1">
            <input type="hidden" name="submitted" value="true">
            <input type="hidden" name="lang" value="de">

            <div class="form-group">
                <p><label>Please log in:</label></p>
                <input class="input-small blue-border" type="text" name="loginname" placeholder="User name" required="required">
            </div>
            <div class="form-group">
                <input class="input-small blue-border" type="password" name="passwd" placeholder="Password" required="required">
            </div>
            <div class="form-group">
                <button type="submit" value="anmelden" class="button button-blue button-small-small">Let me in</button>
            </div>
            <div class="divider"></div>
            <div class="help">
                <small>
                    <a target="_blank" href="https://www.bissantz.com/contact/login_daten_nicht_zur_hand">
                        Can't remember your login details?
                    </a><br/>
                    Please make sure that your browser is configured to accept session cookies.
                </small>
            </div>
        </form>
    </div>
</div>

<script src="https://www.bissantz.com/files/js/js_HTmLUBrwhHFFGew5Kr_eXpcn3oJLU0AZe6Hilj7t-oY.js"></script>

</body>
</html>
